//
//  ContentView.swift
//  SwiftUI_Base
//
//  Created by Techeniac Services on 09/09/21.
//

import SwiftUI

struct SplashView: View {
    @State var isLogin : Bool = false
    @State var counter : Int = 0
    let timer = Timer.publish(every: 1, on: .main, in: .common).autoconnect()
    @State var timerTime : Float
    @State var minute: Float = 0.0
    @State private var showLinkTarget = false
    @State private var shouldNavigate = false

    var body: some View {
            ZStack{
//                VStack{
//                Image("app_icon")
//                    .size(width: 70, height: 70)
//                    Text("Find Me").customFont(size: 26, foregroundColor: AppColor.blackText, weight: .semibold)
//                }
                Image("img_splash")
                    .resizable()
                    .frame(width: 200, height: 250)
            }
            .background(
                NavigationLink(destination: TabBarView(viewRouter: ViewRouter()),
                                  isActive: $shouldNavigate) { EmptyView() }
            )
            .onReceive(timer) { _ in
                if self.minute == self.timerTime {
                    print("Timer completed navigate to Break view")
                    self.timer.upstream.connect().cancel()
                    self.shouldNavigate = true      // << here
                } else {
                    self.minute += 1.0
                }
            }
    }
    func initConfig() {
//        if !isLogin {
//
//        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
//            self.isLogin = true
//            NavigationLink("", destination: LoginView(), isActive: $isLogin)
//
//            print(isLogin)
//        }
//            
//        }
        counter+=1
        print("On Apper",counter)

    }

}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        SplashView(timerTime: 0.0)
    }
}
