//
//  NotificationAlertPopupiView.swift
//  SwiftUI_Base
//
//  Created by Techeniac Services on 19/10/21.
//

import SwiftUI

struct NotificationAlertPopupiView: View {
    @Environment(\.presentationMode) var presentationMode : Binding<PresentationMode>
    @State var isNavigation : Bool = true

    var provider : ProviderModel = ProviderData.sampleData
    var body: some View {
        NavigationView{
            ZStack{
                VStack(spacing: 5.0){
                    VStack{
                        Image(provider.providerImage).size(width: 70, height: 70)
                            .cornerRadius(8)
                        Text(provider.providerName).customFont(size: 14, foregroundColor: AppColor.black, weight: .bold)
                        Text(provider.providerCategory).customFont(size: 10, foregroundColor: AppColor.subTitleGray, weight: .regular)
                    }
                    Divider()
                        .padding(14.0)
                    Text("Hey! do you need any help with \(provider.providerCategory)?")
                        .customFont(size: 18, foregroundColor: AppColor.black, weight: .bold)
                        .fixedSize(horizontal: false, vertical: true)
                        .multilineTextAlignment(.center)
                        .padding(.horizontal, 40.0)
                    Text("\(provider.providerName) is in your area right now, contact him if you need.").customFont(size: 12, foregroundColor: AppColor.subTitleGray, weight: .regular)
                        .fixedSize(horizontal: false, vertical: true)
                        .multilineTextAlignment(.center)
                        .padding(.horizontal, 60.0)

                    Spacer()
                        .frame(height: 30.0)
                    //Flotting Button
                    VStack{
                        Spacer()
                                            NavigationLink(
                                                destination:VStack{ ProviderProfileDetailView(providerDetails: provider).navigationBarHidden(true)},
                                                isActive: $isNavigation){
                        HStack(alignment: .center){
                            Spacer()
                            Text("View Profile")
                                .customFont(size: 17, foregroundColor: AppColor.white, weight: .bold)
                                .frame(height: 50)
                            Spacer()
                        }
                        .onTapGesture {
                            print("View Profile")
                            self.presentationMode.wrappedValue.dismiss()
                        }
                                            }
                        .background(Color.init(AppColor.buttonBackgroundColor))
                        .cornerRadius(10)
                        .padding(.horizontal)
                        Spacer().frame( height: 5)
                    }.frame( height: 50)

                }.padding(.horizontal, 24.0)
            }
        }
    }
}

struct NotificationAlertPopupiView_Previews: PreviewProvider {
    static var previews: some View {
        NotificationAlertPopupiView()
    }
}
