//
//  BookingHistoryView.swift
//  SwiftUI_Base
//
//  Created by Techeniac Services on 19/10/21.
//

import SwiftUI
import StarRating

struct BookingHistoryView: View {
    @Environment(\.presentationMode) var presentationMode : Binding<PresentationMode>
    @State private var CategoryCellSize: [GridItem] = [GridItem(.flexible())]
    @State var isNavigation : Bool = false
    @State private var selectedType : String = "Upcoming"
    @State private var bookingType : [String] = ["Upcoming","Complete"]

    var orderDetailsList : [ProviderModel] = ProviderData.ProviderList

    
    var body: some View {
        NavigationView{
            VStack(spacing: 20.0){
                VStack{
                    Picker("Favorite Color", selection: $selectedType, content: {
                        ForEach(bookingType, id: \.self, content: { type in
                            Text(type)
                        })
                    })
                    .pickerStyle(SegmentedPickerStyle())
                    .background(Color.init(AppColor.lightBlueBackgroundColor))
//                    .padding(.horizontal, 10.0)
                    .onChange(of: selectedType, perform: { type in
                        if type == bookingType[0]{
                            
                            print(type)
                        }else{
                            print(type)
                        }
                    })
                }
                
                ScrollView{
                    ForEach(orderDetailsList, id: \.self){ order in
                    BookingCellBlock(providerDetails: order)
                    }
                }.background(Color.init(AppColor.white))
                .cornerRadius(8)
//                .padding()
            }.padding(.all, 20.0)
            
            .navigationBarTitleDisplayMode(.inline)
//            .navigationBarItems(leading:
//                                    Button(action: {
//                                        print("Back")
//                                        self.presentationMode.wrappedValue.dismiss()
//                                    }
//                                    ){
//                                        Image("back")
//                                            .size(width: 30, height: 30, alignment: .center)
//                                    }
//            )
            .toolbar { // <2>
                ToolbarItem(placement: .principal) {
                    Text("Booking History")
                        .customFont(size: 16,
                                    foregroundColor: AppColor.black,
                                    weight: .bold)
                }
            }
            .background(Color.init(AppColor.AppBackgroundColor).ignoresSafeArea())
            
        }
    }
}

struct BookingHistoryView_Previews: PreviewProvider {
    static var previews: some View {
        BookingHistoryView()
    }
}

struct BookingCellBlock: View {
    var providerDetails : ProviderModel
    @State var rtRatingStar = StarRatingConfiguration(numberOfStars: 5, stepType: .exact, minRating: 0, borderColor: Color.clear,emptyColor: Color.init(AppColor.rtRatingStarEmptyColor),shadowColor: Color.clear, fillColors: [Color.init(AppColor.rtRatingStarFillColor)])

    var body: some View {
        VStack(alignment: .leading){
            HStack(alignment: .top){
                HStack{
                    
                    Image("Plumbing").size(width: 16, height: 16)
                        .padding(8.0)
                }
                .frame(width: 33, height: 33)
                .background(Color.init(AppColor.lightBlueBackgroundColor))
                .cornerRadius(5)
                VStack(alignment: .leading, spacing: 3.0){
                    ZStack{
                        
                    }.frame(height: 1)
//                    .st
                    HStack{
                        Text("Excellent Care").customFont(size: 14, foregroundColor: AppColor.black, weight: .bold)
                        Text("(Plumbing Service)").customFont(size: 14, foregroundColor: AppColor.subTitleGray, weight: .regular)
                        Spacer()
                    }
                    HStack{
                        Text("Scheduled for ").customFont(size: 14, foregroundColor: AppColor.subTitleGray, weight: .regular)
                        Spacer()
                        
                        Text("Fri 23rd May, 3:00 PM").customFont(size: 10, foregroundColor: AppColor.blackText, weight: .medium)
                    }
                    HStack{
                        Text("Estimated cost").customFont(size: 14, foregroundColor: AppColor.subTitleGray, weight: .regular)
                        Spacer()
                        
                        Text("$45.00").customFont(size: 10, foregroundColor: AppColor.blackText, weight: .medium)
                    }
                }
            }
            HStack(alignment: .top){
                Image(providerDetails.providerImage).size(width: 33, height: 33)
                    .cornerRadius(5)
                VStack(alignment: .leading, spacing: 3.0){
                    Text(providerDetails.providerName).customFont(size: 14, foregroundColor: AppColor.black, weight: .bold)
                    HStack(spacing: 3.0){
                        Image("experiance").size(width: 14, height: 14, alignment: .center)
                        Text("\(providerDetails.providerExperiance) \(providerDetails.providerExperiance == "1" ? "Year" : "Years") of experiance")
                            .customFont(size: 10, foregroundColor: AppColor.subTitleGray)
                    }
                    HStack(spacing: 3.0){
                        Image("ic_call_gray").size(width: 14, height: 14, alignment: .center)
                        Text("Contact ")
                            .customFont(size: 10, foregroundColor: AppColor.subTitleGray)
                        Text(providerDetails.providerContactNumber)
                            .customFont(size: 10, foregroundColor: AppColor.lightBlueText)
                    }
                    HStack(spacing: 1){
                        Text(providerDetails.providerRateing)
                            .customFont(size: 10, foregroundColor: AppColor.rtRatingStarFillColor,weight: .bold)
                        StarRating(initialRating: Double(providerDetails.providerRateing) ?? 0.0,configuration: $rtRatingStar)
                            .offset(x: -10)
                    }
                }
            }
            Divider()
                .padding(.top)
        }.padding()
    }
}
