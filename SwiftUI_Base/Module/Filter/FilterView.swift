//
//  FilterView.swift
//  SwiftUI_Base
//
//  Created by Techeniac Services on 04/10/21.
//

import SwiftUI
enum shortBy{
    case TopRated
    case Cost_High_To_Low
    case Cost_Low_To_High
    var description : String{
        switch self {
        case .TopRated: return "Top Rated"
        case .Cost_High_To_Low: return "Cost High To Low"
        case .Cost_Low_To_High: return "Cost Low To High"
        }
    }
}
struct FilterView: View {
    @Environment(\.presentationMode) var presentationMode : Binding<PresentationMode>
    @State private var isNavigation : Bool = false
    @State private var txtLocation : String = ""
    @State private var txtMinPrice : String = "0"
    @State private var txtMaxPrice : String = "100"
    @State private var selectedShortBy : shortBy? = .TopRated
    @State private var sortByList : [shortBy] = [.TopRated,.Cost_High_To_Low,.Cost_Low_To_High]
    @State private var serviceList : [String] = ["All","Plumber","Carpentor","HandyMan","Cleaning","House Cleaning","Kitchen Cleaning","Meachanic","Car wash","Painting","Washing"]
    @State private var selectedServiceList : [String] = []
    
    //    @State var op: Color?
    
    
    var body: some View {
        NavigationView{
            VStack{
                ScrollView{
                    VStack(spacing: 30.0){
                        //Location
                        VStack(alignment: .leading){
                            Text("Location")
                                .customFont(size: 16, foregroundColor: AppColor.black, weight: .bold)
                            Group{
                                TextField("Select near by area", text: $txtLocation)
                                    .background(Color.white)
                                    .frame( height: 50)
                                //                                .keyboardType(.numberPad)
                            }
                            .padding(.horizontal)
                            .background(Color.init(AppColor.white))
                            .frame( height: 50)
                            .cornerRadius(10)
                            .overlay(RoundedRectangle(cornerRadius: 10).stroke(lineWidth: 2).foregroundColor(Color.init(AppColor.borderColor).opacity(0.2)))
                            
                        }
                        .padding(.top, 20.0)
                        //Price Range per Hour
                        VStack(alignment: .leading){
                            HStack(spacing: 3.0) {
                                Text("Price Range")
                                    .customFont(size: 16, foregroundColor: AppColor.black, weight: .bold)
                                
                                Text("/ Per hr")
                                    .customFont(size: 12, foregroundColor: AppColor.black, weight: .regular)
                            }
                            
                            HStack(spacing: 20.0) {
                                Group{
                                    TextField("$ Min", text: $txtMinPrice)
                                        //                                    TextField("$ Min", text: .constant(""))
                                        .background(Color.white)
                                        .frame( height: 50)
                                        .keyboardType(.numberPad)
                                }
                                .padding(.horizontal)
                                .background(Color.init(AppColor.white))
                                .frame( height: 50)
                                .cornerRadius(10)
                                .overlay(RoundedRectangle(cornerRadius: 10).stroke(lineWidth: 2).foregroundColor(Color.init(AppColor.borderColor).opacity(0.2)))
                                
                                Group{
                                    TextField("$ Max", text: $txtMaxPrice)
                                        //                                    TextField("$ Max", text: .constant(""))
                                        .background(Color.white)
                                        .frame( height: 50)
                                        .keyboardType(.numberPad)
                                }
                                .padding(.horizontal)
                                .background(Color.init(AppColor.white))
                                .frame( height: 50)
                                .cornerRadius(10)
                                .overlay(RoundedRectangle(cornerRadius: 10).stroke(lineWidth: 2).foregroundColor(Color.init(AppColor.borderColor).opacity(0.2)))
                            }
                        }
                        // Sort BY
                        VStack(alignment: .leading){
                            Text("Sort By")
                                .customFont(size: 16, foregroundColor: AppColor.black, weight: .bold)
                            VStack(spacing: 0) {
                                ForEach(sortByList, id:  \.self) { service in
                                    ShortByRadioCellView(selectedShortList: $selectedShortBy, sortList: service)
                                }
                            }
                        }
                        // Services
                        VStack(alignment: .leading){
                            Text("Services")
                                .customFont(size: 16, foregroundColor: AppColor.black, weight: .bold)
                            FlexibleView(
                                data: serviceList,
                                spacing: 15,
                                alignment: .leading
                            ) { item in
                                Text(verbatim: item)
                                    .padding(6)
                                    .foregroundColor(selectedServiceList.contains(item) ? Color.init(AppColor.white): Color.init(AppColor.subTitle))
                                    .background(
                                        RoundedRectangle(cornerRadius: 8)
                                            .fill(selectedServiceList.contains(item) ? Color.init(AppColor.blackText) : Color.init(AppColor.lightBlueBackgroundColor))
                                    )
                                    .onTapGesture {
                                        print(selectedServiceList)
                                        if item == "All"{
                                            if selectedServiceList.contains(item){
                                                selectedServiceList.removeAll()
                                            }else{
                                                selectedServiceList = serviceList
                                            }
                                        }else{
                                            if selectedServiceList.contains("All"){
                                                selectedServiceList.remove(at: selectedServiceList.firstIndex(of: "All")!)
                                            }
                                            if selectedServiceList.contains(item) {
                                                if let index = selectedServiceList.firstIndex(of: item){
                                                    selectedServiceList.remove(at: index)
                                                }
                                            }else{
                                                selectedServiceList.append(item)
                                            }
                                            if selectedServiceList.count == (serviceList.count - 1){
                                                selectedServiceList.append("All")
                                            }
                                        }
                                    }
                            }
                        }
                    }
                    .padding(.horizontal, 20.0)
                }
                VStack{
                    Spacer()
                    NavigationLink(
                        destination:VStack{ SearchView().navigationBarHidden(true)},
                        isActive: $isNavigation){
                        HStack(alignment: .center){
                            Spacer()
                            Text("Apply")
                                .customFont(size: 17, foregroundColor: AppColor.white, weight: .bold)
                                .frame(height: 50)
                            Spacer()
                        }
                    }
                    .background(Color.init(AppColor.buttonBackgroundColor))
                    .cornerRadius(10)
                    .padding(.horizontal)
                    Spacer().frame( height: 5)
                }.frame(height: 50)
            }
            
            .navigationBarTitleDisplayMode(.inline)
            .navigationBarItems(leading:
                                    Button(action: {
                                        print("Back")
                                        self.presentationMode.wrappedValue.dismiss()
                                    }
                                    ){
                                        Image("back")
                                            .size(width: 30, height: 30, alignment: .center)
                                    }
            )
            .navigationBarItems(trailing:                                                               Button(action: {
                                        reset()
                                    }
                                    ){
                                        Text("Reset").customFont(size: 16, foregroundColor: AppColor.blackText, weight: .medium)
                                    }
            )
            .toolbar { // <2>
                ToolbarItem(placement: .principal) {
                    Text("Sort and Filters")
                        .customFont(size: 16,
                                    foregroundColor: AppColor.black,
                                    weight: .bold)
                }
            }
            .background(Color.init(AppColor.AppBackgroundColor).ignoresSafeArea())
        }
    }
    func reset(){
        print("Reset")
        txtLocation = ""
        txtMinPrice = ""
        txtMaxPrice = ""
        selectedShortBy = .TopRated
        sortByList = [.TopRated,.Cost_High_To_Low,.Cost_Low_To_High]
        selectedServiceList.removeAll()
        print(isNavigation)
    }
}

struct ShortByRadioCellView: View {
    
    @Binding var selectedShortList: shortBy?
    var sortList: shortBy
    
    var body: some View {
        VStack(spacing: 0.0) {
            HStack(spacing: 10.0){
                Button(action: {
                    self.selectedShortList = self.sortList
                }) {
                    Image(self.selectedShortList == self.sortList ? "ic_radio_checked" : "ic_radio_unchecked")
                    Text("\(self.sortList.description)").customFont(size: 14, foregroundColor: selectedShortList == sortList ? AppColor.blackText : AppColor.subTitle , weight: .medium)
                    Spacer()
                }
            }
            .padding(.horizontal)
            .frame(height: 50.0)
            .background(Color.init(self.selectedShortList == self.sortList ? AppColor.lightBlueBackgroundColor : AppColor.white))
            .cornerRadius(10)
            //            Divider().padding(.horizontal)
        }
        .overlay(RoundedRectangle(cornerRadius: 10).stroke(lineWidth: 2).foregroundColor(Color.init(AppColor.borderColor).opacity(0.2)))
        
        //        .accentColor(self.color)
    }
}

struct FilterView_Previews: PreviewProvider {
    static var previews: some View {
        FilterView()
        //        ShortByRadioCellView(selected: .Cost_Low_To_High, sortList: .TopRated)
    }
}
