//
//  BookingSlotView.swift
//  SwiftUI_Base
//
//  Created by Techeniac Services on 08/10/21.
//

import SwiftUI
//import UIKit
struct BookingSlotView: View {
    @State var isNavigation : Bool = false
    @Environment(\.presentationMode) var presentationMode : Binding<PresentationMode>
    @State private var dayBlockSize: [GridItem] = [GridItem(.flexible())]
    @State private var timeBlockSize: [GridItem] = [GridItem(.flexible()),GridItem(.flexible()),GridItem(.flexible())]
    
    @State private var greetingList: [String] = ["Morning","Afternoon","Evening"]
    @State private var monthList: [String] = ["January","February","March","April","May","June","July","August","September","October","November","December"]
    @State private var slotList: [String] = ["1:00PM","1:30PM","2:00PM","2:30PM","3:00PM","3:30PM"]
    @State private var selectedSlot: String = "1:00PM"
    @State private var selectedDate =  Date()
    @State private var selectedMonth: Int = 0
    @State private var selectedGreeting : String = "Morning"
    let date = Date()
    
    var body: some View {
        NavigationView{
            VStack {
                ScrollView{
                    Spacer().frame(height: 19)
                    VStack(spacing: 20){
                        HStack(spacing: 10.0){
                            //                            Text("\(monthList[selectedMonth])")
                            Text(selectedDate.formatDate(format: "MMMM"))
                            Text("")
                            Spacer()
                            Button(action: {
                                // remove 1 Month
                                //  selectedMonth -= 1
                                //  if selectedMonth <= 0 {selectedMonth = 11}
                                
                                selectedDate  = Calendar.current.date(byAdding: .day, value: -1, to: selectedDate)!
                            }, label: {
                                //                                Text("<")
                                Image("ic_left").size(width: 22, height: 22)
                            })
                            Button(action: {
                                // add 1 Month
                                //  selectedMonth += 1
                                //  if selectedMonth >= 11 {selectedMonth = 0}
                                
                                selectedDate  = Calendar.current.date(byAdding: .day, value: 1, to: selectedDate)!
                            }, label: {
                                //                                Text(">")
                                Image("ic_right").size(width: 22, height: 22)
                            })
                        }
                        .padding([.top, .leading, .trailing], 10.0)
                        ZStack{
                            Divider().padding(.horizontal, 10.0).offset(y: -40)
                            Divider().padding(.horizontal, 10.0).offset(y: 40)
                            
                            
                            
                            ScrollView(.horizontal,showsIndicators: false) {
                                HStack(spacing: 18){
                                    ForEach (0..<50){ day in
                                        let date  = Calendar.current.date(byAdding: .day, value: day, to: Date())!
                                        if selectedDate.formatDate(format: "dd") == "\(date.formatDate(format: "dd"))"{
                                            Capsule()
                                                .fill(Color.init(AppColor.lightBlueBackgroundColor))
                                                .overlay(
                                                    Capsule()
                                                        .stroke(Color.init(AppColor.lightBlueText),lineWidth: 1)
                                                )
                                                .frame(width: 45, height: 80)
                                                .overlay(
                                                    DateView(date: date)
                                                )
                                                .onTapGesture {
                                                    print("\(date.formatDate(format: "EEE"))")
                                                    print("\(date.formatDate(format: "dd"))")
                                                    print("\(selectedDate.formatDate(format: "MMMM"))")
                                                    selectedDate = date
                                                }
                                        }else{
                                            DateView(date: date)
                                                
                                                .frame(width: 45, height: 80)
                                                
                                                .onTapGesture {
                                                    print("\(date.formatDate(format: "EEE"))")
                                                    print("\(date.formatDate(format: "dd"))")
                                                    print("\(selectedDate.formatDate(format: "MMMM"))")
                                                    selectedDate = date
                                                }
                                            
                                        }
                                    }
                                }
                            }
                        }
                        .frame(height: 80.0)
                        Picker("Favorite Color", selection: $selectedGreeting, content: {
                            ForEach(greetingList, id: \.self, content: { greeting in
                                Text(greeting)
                            })
                        })
                        .pickerStyle(SegmentedPickerStyle())
                        .background(Color.init(AppColor.lightBlueBackgroundColor))
                        .padding(.horizontal, 10.0)
                        .onChange(of: selectedGreeting, perform: { value in
                            if value == greetingList[0]{
                                slotList.append("123")
                            }else if value == greetingList[1]{
                                slotList.removeLast()
                            }else{
                                slotList.removeFirst()
                                
                            }
                        })
                        VStack(alignment: .leading){
                            Text("Select Time").customFont(size: 14, foregroundColor: AppColor.black, weight: .bold)
                            LazyVGrid(columns: timeBlockSize, spacing: 20) {
                                ForEach(slotList, id: \.self) { item in
                                    VStack{
                                        Text("\(item)").customFont(size: 14, foregroundColor: selectedSlot != item ? AppColor.black : AppColor.white, weight: .medium)
                                    }
                                    .frame(height: 40.0)
                                    .frame(minWidth: 100)
                                    .background(Color.init(selectedSlot != item ? AppColor.lightBlueBackgroundColor : AppColor.black))
                                    .cornerRadius(3)
                                    .onTapGesture {
                                        selectedSlot = item
                                    }
                                }
                            }
                        }
                        .padding(.horizontal, 10.0)
                        Divider()
                            .padding(.horizontal, 10.0)
                        HStack(spacing: 20.0){
                            HStack{
                                Image("selected_slot").size(width: 16, height: 16)
                                Text("Selected Slot").customFont(size: 14, foregroundColor: AppColor.subTitleDark, weight: .medium)
                            }
                            HStack{
                                Image("un_selected_slot").size(width: 16, height: 16)
                                Text("Available Slot").customFont(size: 14, foregroundColor: AppColor.subTitleDark, weight: .medium)
                            }
                        }
                        .padding(.bottom, 10.0)
                    }
                    .background(Color.init(AppColor.white))
                    //                    .background(Color.red)
                    .padding([.leading, .bottom, .trailing], 20)
                    .cornerRadius(10)
                }
                .edgesIgnoringSafeArea(.bottom)
                //Floting Button
                VStack{
                    Spacer()
                    HStack(alignment: .center){
                        Spacer()
                        NavigationLink(
                            destination: AddressListView(addressList: AddressData.addressList).navigationBarHidden(true),
                            isActive: $isNavigation,
                            label: {
                                
                                HStack(alignment: .bottom, spacing: 1.0) {
                                    Text("Select Address")
                                        .customFont(size: 17, foregroundColor: AppColor.white, weight: .bold)
                                }.padding(.horizontal, 14)
                            })
                            .frame(height: 50)
                        Spacer()
                    }
                    .background(Color.init(AppColor.buttonBackgroundColor))
                    .cornerRadius(10)
                    .padding(.horizontal)
                    Spacer().frame( height: 5)
                }.frame(height: 50)
            }
            .navigationBarTitleDisplayMode(.inline)
            .navigationBarItems(leading:
                                    Button(action: {
                                        print("Back")
                                        self.presentationMode.wrappedValue.dismiss()
                                    }
                                    ){
                                        Image("back")
                                            .size(width: 30, height: 30, alignment: .center)
                                    }
            )
            .toolbar { // <2>
                ToolbarItem(placement: .principal) {
                    Text("Booking Slot")
                        .customFont(size: 16,
                                    foregroundColor: AppColor.black,
                                    weight: .bold)
                }
            }
            .background(Color.init(AppColor.AppBackgroundColor).ignoresSafeArea())
        }
    }
}

struct BookingSlotView_Previews: PreviewProvider {
    static var previews: some View {
        BookingSlotView()
    }
}
extension Binding {
    func onChange(_ handler: @escaping (Value) -> Void) -> Binding<Value> {
        return Binding(
            get: { self.wrappedValue },
            set: { selection in
                self.wrappedValue = selection
                handler(selection)
            })
    }
}

//struct DatePickerView: View {
//    var modifiredDate = Date()
//    let numberOfday = 50
//    var body: some View{
//        let now = Date()
//        //        let modifiredDate = Calendar.current.date(byAdding: .day, value: 1, to: now)!
//
//        return ScrollView{
//            HStack{
//                ForEach (1..<numberOfday){ day in
//                    let date  = Calendar.current.date(byAdding: .day, value: day, to: now)!
//                    VStack{
//                        Text("\(Calendar.current.dateComponents([.day], from: date).day!)")
//                            .customFont(size: 12, foregroundColor: AppColor.subTitleDark, weight: .medium).id("\(Calendar.current.dateComponents([.day], from: date).day!)")
//                        Spacer()
//                        Text(String(format: "%02d", "\(Calendar.current.dateComponents([.day], from: date).day!)")).customFont(size: 16, foregroundColor: AppColor.black, weight: .bold)
//
//                    }
//                }
//            }
//        }
//    }
//}

struct DateView: View {
    var date : Date
    var body: some View {
        VStack{
            Spacer()
            Text("\(date.formatDate(format: "EEEEE"))")
                .customFont(size: 12, foregroundColor: AppColor.subTitleDark, weight: .medium).id("\(Calendar.current.dateComponents([.weekday], from: date).weekday!)")
            Spacer()
            Text("\(date.formatDate(format: "dd"))").customFont(size: 16, foregroundColor: AppColor.black, weight: .bold)
            Spacer()
        }
    }
}
