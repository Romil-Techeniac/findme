//
//  LocationView.swift
//  SwiftUI_Base
//
//  Created by Techeniac Services on 29/09/21.
//

import SwiftUI
import MapKit

struct PinItem: Identifiable {
    let id = UUID()
    let coordinate: CLLocationCoordinate2D
}

struct LocationView: View {
    @StateObject var locationManager = LocationManager()
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @State private var isNavigation : Bool = false
    @State var cityName: String = ""
    
    var userLatitude: String {
        return "\(locationManager.lastLocation?.coordinate.latitude ?? 0)"
    }
    
    var userLongitude: String {
        return "\(locationManager.lastLocation?.coordinate.longitude ?? 0)"
    }
    
    var TopRatedImage: [String] = ["work_1","work_2"]
    var rows: [GridItem] = [GridItem(.flexible())]
    private enum MapDefaults {
        static let latitude = 37.334722//45.872
        static let longitude = -122.008889//-1.248
        static let zoom = 0.005
    }
    
    @State private var region1: MKCoordinateRegion = MKCoordinateRegion(
        center: CLLocationCoordinate2D(latitude: CLLocationDegrees(/*LocationManager().lastLocation?.coordinate.latitude ?? */MapDefaults.latitude), longitude: CLLocationDegrees(/*LocationManager().lastLocation?.coordinate.longitude ?? */MapDefaults.longitude)),
        span: MKCoordinateSpan(latitudeDelta: MapDefaults.zoom, longitudeDelta: MapDefaults.zoom))
    //    var annotationItems: [MyAnnotationItem] = [
    //        MyAnnotationItem(coordinate: CLLocationCoordinate2D(latitude: 37.810000, longitude: -122.477450)),
    //        MyAnnotationItem(coordinate: CLLocationCoordinate2D(latitude: 37.786996, longitude: -122.419281)),
    //    ]
    
    var body: some View {
        let provider : ProviderModel = ProviderData.sampleData
        NavigationView{
            GeometryReader { p in
                ZStack{
                    //                    Map(coordinateRegion: $region1)
                    Map(coordinateRegion: $region1,
                        //                    interactionModes: [],
                        //                    showsUserLocation: true,
                        //                    userTrackingMode: nil,
                        annotationItems: [PinItem(coordinate: .init(latitude: 37.334722, longitude: -122.008889)),PinItem(coordinate: .init(latitude: 37.335722, longitude: -122.00789))]) { item in
                        MapAnnotation(coordinate: item.coordinate){
                            Image("rate").size(width: 25, height: 25).onTapGesture {
                                print("Click on finder",item.coordinate)
                            }
                        }
                        
                    }
                    .edgesIgnoringSafeArea(.all)
                    Group {
                        VStack{
                            Group{
                                HStack {
                                    Image("search").size(width: 15, height: 15, alignment: .center)
                                    TextField("Search your location", text: .constant(""))
                                        .background(Color.white)
                                        .frame( height: 50)
                                }
                                .padding(.horizontal)
                                .background(Color.init(AppColor.white))
                                .frame( height: 50)
                                .cornerRadius(10)
                                .overlay(RoundedRectangle(cornerRadius: 10).stroke(lineWidth: 2).foregroundColor(Color.init(AppColor.borderColor).opacity(0)).shadow(color:Color.init(AppColor.borderColor), radius: 10).opacity(0.8))
                            }
                            Spacer()
                            // User pointer Location
                            Text("\(region1.center.latitude)")
                            Text("\(region1.center.longitude)")
                            Text("\(region1.span.latitudeDelta)")
                            if false{
                                ScrollView(.horizontal, showsIndicators: false) {
                                    LazyHGrid(rows: rows, alignment: .top) {
                                        ForEach(TopRatedImage, id: \.self) { TopBannerImage in
                                            NavigationLink(
                                                destination: BookingDetailsView(id: "\(TopBannerImage)").navigationBarHidden(true), // <1>
                                                label: {
                                                    
                                                    TopBannerView(imageName: TopBannerImage
                                                    )
                                                    .onTapGesture {
                                                        print(TopBannerImage)
                                                    }
                                                })
                                        }
                                    }.frame(height: 160)
                                }
                            }
                            Spacer().frame( height: 70)
                            if true{
                                VStack {
                                    HStack{
                                        Button(action: {
                                            print("ic_a_to_b_point")
                                        }) {
                                            Image("ic_a_to_b_point")
                                                .size(width: 22, height: 90)
                                        }
                                        VStack(alignment: .leading){
                                            VStack(alignment: .leading, spacing: 0.0){
                                                Text("From").customFont(size: 10, foregroundColor: AppColor.subTitle, weight: .medium)
                                                    Text("Service Provider Location").customFont(size: 14, foregroundColor: AppColor.blackText, weight: .medium)
//                                                Spacer()
                                            }
                                            Divider()
                                            VStack{
                                                VStack(alignment: .leading, spacing: 0.0){
                                                    Text("To").customFont(size: 10, foregroundColor: AppColor.subTitle, weight: .medium)
                                                        Text("142, Titanium Square, Thaltej...").customFont(size: 14, foregroundColor: AppColor.blackText, weight: .medium)
//                                                    Spacer()

                                                }
                                            }
                                        }
                                        Button(action: {
                                            print("ic_a_to_b_point")
                                        }) {
                                            Image("call")
                                                .size(width: 40, height: 40)
                                        }

                                    }
                                    HStack{
                                        Circle()
                                            .trim(from: 0.25, to: 0.75)
                                            .fill(Color.gray)
                                            .rotationEffect(.degrees(180))
                                            .frame(width: 24, height: 24)
                                            .offset(x: -12)
                                        Text("----------------------------").fixedSize(horizontal: true, vertical: true)

                                        Circle()
                                            .trim(from: 0.25, to: 0.75)
                                            .fill(Color.gray)
                                            .frame(width: 24, height: 24)
                                            .offset(x: 12)


                                    }.background(Color.yellow)
                                    HStack{
                                        Image(provider.providerImage).size(width: 50, height: 50, alignment: .center).cornerRadius(5)
                                        Spacer().frame(width: 10)
                                        VStack(alignment: .leading){
                                            Spacer()
                                            Text(provider.providerName)
                                                .customFont(size: 14, foregroundColor: AppColor.black, weight: .bold)
                                            Spacer()
                                            
                                            Text(provider.providerCategory)
                                                .customFont(size: 10, foregroundColor: AppColor.subTitleGray)
                                                .fixedSize(horizontal: false, vertical: true)
                                            Spacer()
                                            
                                        }
                                        Spacer()
                                        VStack(alignment: .trailing){
                                            HStack{
                                                Image("message")
                                                    .size(width: 40, height: 40, alignment: .center)
                                                    .cornerRadius(10)
                                                    .onTapGesture {
                                                        print(provider.providerChatID)
                                                    }
                                                Image("call")
                                                    .size(width: 40, height: 40, alignment: .center)
                                                    .cornerRadius(10)
                                                    .onTapGesture {
                                                        print(provider.providerContactNumber)
                                                    }
                                            }
                                        }
                                        .padding([.top,.bottom],10)
                                    }.frame(height: 30, alignment: .center)
                                }
                                .padding(16.0)
                                .background(Color.white)
                                .frame(height: 200.0)
                                
                            }
                            Spacer().frame( height: 80)
                            
                        }
                    }.padding(.horizontal,20)
                    
                    //Floting Button
                    VStack(alignment: .center){
                        Spacer()
                        HStack(alignment: .center, spacing: 15.0){
                            VStack{
                                Spacer()
                                //                                NavigationLink(
                                //                                    destination:VStack{ Text("Destination")},
                                //                                    isActive: $isNavigation){
                                Button(action: {
                                    presentationMode.wrappedValue.dismiss()
                                }){
                                    HStack(alignment: .center){
                                        Spacer()
                                        Text("Proceed to CheckOut")
                                            .customFont(size: 17, foregroundColor: AppColor.white, weight: .bold)
                                            .frame(height: 50)
                                        Spacer()
                                    }
                                }
                                //                                }
                                .background(Color.init(AppColor.buttonBackgroundColor))
                                .cornerRadius(10)
                                .padding(.horizontal)
                                Spacer().frame( height: 5)
                            }
                            .background(Color.init(AppColor.buttonBackgroundColor))
                            .cornerRadius(10)
                            Image("ic_current_location").size(width: 50, height: 50).onTapGesture {
                                print("Get Current Location",userLatitude ,userLongitude)
                                region1 = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: CLLocationDegrees(userLatitude) ?? MapDefaults.latitude, longitude: CLLocationDegrees(userLongitude) ?? MapDefaults.longitude), span: MKCoordinateSpan(latitudeDelta: 0.005, longitudeDelta: 0.005))
                            }
                        }
                        .frame( height: 50)
                    }
                    .padding(.horizontal,20.0)
                    .padding(.bottom,5)
                    //Current Location Pin
                    Group{
                        Image("ic_location_pin").size(width: 50, height: 50)
                    }
                }
                .navigationBarHidden(true)
            }
        }
    }
    func GetCity(){
        let name = CLLocation(latitude:region1.center.latitude, longitude: region1.center.longitude)
        name.fetchCityAndCountry { (city, country, zipcode, error) in
            print(city, country, error as Any)
            cityName = city
        }
    }
    
}

struct LocationView_Previews: PreviewProvider {
    static var previews: some View {
        LocationView()
    }
}
