//
//  BookingDetailsView.swift
//  SwiftUI_Base
//
//  Created by Techeniac Services on 15/09/21.
//

import SwiftUI
import UIKit
struct BookingDetailsView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @State var id : String = ""
    @State var isNavigation : Bool = false
    @State var selctedAddress : String = "2278  Dogwood Lane,  Saint David,\nArizona\nAZ-3822565"
    @State var providerLat : String = ""
    @State var providerLong : String = ""
    var providerDetails : ProviderModel = ProviderData.ProviderList[1]
    
    var body: some View {
        NavigationView{
            VStack{
                ScrollView{
                    VStack{
                        Spacer().frame(height: 19)
                        Group{
                            VStack{
                                //AddressView
                                AddressView(selectedAddress: $selctedAddress)
                                Spacer()
                                    .frame(height: 30.0)
                                //ServiceProvider View
                                SearviceProviderView(providerDetails: providerDetails)
                            }
                        }
                    }
                }
                .edgesIgnoringSafeArea(.bottom)
                
                //Floting Button
                VStack{
                    Spacer()
                    NavigationLink(
                        destination:VStack{ CheckoutView().navigationBarHidden(true)},
                        isActive: $isNavigation){
                        HStack(alignment: .center){
                            Spacer()
                            Text("Proceed to CheckOut")
                                .customFont(size: 17, foregroundColor: AppColor.white, weight: .bold)
                                .frame(height: 50)
                            Spacer()
                        }
                    }
                    .background(Color.init(AppColor.buttonBackgroundColor))
                    .cornerRadius(10)
                    .padding(.horizontal)
                    Spacer().frame( height: 5)
                }.frame(height: 50)
            }
            .navigationBarTitleDisplayMode(.inline)
            .navigationBarItems(leading:
                                    Button(action: {
                                        print("Back")
                                        self.presentationMode.wrappedValue.dismiss()
                                    }
                                    ){
                                        Image("back")
                                            .size(width: 30, height: 30, alignment: .center)
                                    }
            )
            .toolbar { // <2>
                ToolbarItem(placement: .principal) {
                    Text("Booking Details")
                        .customFont(size: 16,
                                    foregroundColor: AppColor.black,
                                    weight: .bold)
                }
            }
            .background(Color.init(AppColor.AppBackgroundColor).ignoresSafeArea())
        }.onAppear{
            print(id)
        }
    }
}



struct AddressView: View {
    @Binding var selectedAddress : String
    var body: some View {
        Group{
            VStack(alignment: .leading){
                HStack{
                    Text("Address")
                        .customFont(size: 14, foregroundColor: AppColor.black, weight: .bold)
                    Spacer()
                    Image("edit")
                        .size(width: 25, height: 25)
                        .onTapGesture {
                            print("Edit address")
                        }
                }
                //                            .frame(height: 80, alignment: .center)
                .padding([.top, .leading, .trailing])
                Divider()
                    .padding(.horizontal)
                VStack(alignment: .leading){
                    Text("Your service provider will be come here.")
                        .customFont(size: 14, foregroundColor: AppColor.subTitle, weight: .bold)
                    Spacer()
                        .frame(height: 10.0)
                    Text("\(selectedAddress)")
                        .customFont(size: 12, foregroundColor: AppColor.subTitle)
                        .lineLimit(nil)
                }
                .padding(.horizontal)
                
                Divider().padding(.horizontal)
                HStack{
                    NavigationLink(
                        destination: Text("Location Screen").navigationBarHidden(false),
                        label: {
                            Image("location_link")
                                .size(width: 15, height:15)
                            Text("Choose Location")
                                .customFont(size: 14, foregroundColor: AppColor.lightBlueText, weight: .medium)
                                .underline()
                        })
                    Spacer()
                }
                .padding(.all)
            }
        }
        .background(Color.white)
        .cornerRadius(10)
        .padding([.leading,.trailing],16)
    }
}

struct SearviceProviderView: View {
    var providerDetails : ProviderModel
    
    var body: some View {
        Group{
            VStack{
                //Service Provier Profile
                ServiceProviderFullProfileView(providerDetails: providerDetails)
                Divider().padding(.horizontal)
                //Service Provier Ratio
                ProviderRateView(providerDetails: providerDetails)
                Divider().padding(.horizontal)
                Group{
                    BookingSlotPreviewView()
                }.padding(.horizontal)
                ProviderContactDetailsView(providerDetails: providerDetails)
            }
            .background(Color.white)
            .cornerRadius(10)
        }
        .padding(.horizontal,16)
        .frame(minHeight: 50, maxHeight: .infinity)
    }
}

struct ProviderRateView: View {
    var providerDetails : ProviderModel
    
    var body: some View {
        HStack{
            Spacer()
            Group{
                HStack{
                    Spacer()
                    VStack(alignment: .leading){
                        Spacer()
                        Text("$\(providerDetails.providerPricePerHours)")
                            .customFont(size: 12, foregroundColor: AppColor.blackText, weight: .bold)
                        Spacer()
                        Text("Hourly Rate")
                            .customFont(size: 10, foregroundColor: AppColor.subTitleGray)
                        Spacer()
                    }
                    Spacer()
                }
            }
            Divider()
            Group{
                HStack{
                    Spacer()
                    VStack(alignment: .leading){
                        Spacer()
                        Text("\(providerDetails.providerSuccsessRate)%")
                            .customFont(size: 12, foregroundColor: AppColor.lightBlueText, weight: .bold)
                        Spacer()
                        Text("Success Rate")
                            .customFont(size: 10, foregroundColor: AppColor.lightBlueText)
                        Spacer()
                    }
                    Spacer()
                }
            }
            Divider()
            Group{
                HStack{
                    Spacer()
                    VStack(alignment: .leading){
                        Spacer()
                        Text("\(providerDetails.providerJobs)+")
                            .customFont(size: 12, foregroundColor: AppColor.blackText, weight: .bold)
                        Spacer()
                        Text("Jobs Done")
                            .customFont(size: 10, foregroundColor: AppColor.subTitleGray)
                        Spacer()
                    }
                    Spacer()
                }
            }
        }
    }
}

struct ProviderContactDetailsView: View {
    var providerDetails : ProviderModel
    
    var body: some View {
        HStack{
            VStack(alignment: .leading){
                Text("Contact Details")
                    .customFont(size: 12, foregroundColor: AppColor.black, weight: .bold)
                Spacer()
                Text("Call : \(providerDetails.providerContactNumber)")
                    .customFont(size: 10, foregroundColor: AppColor.subTitleGray, weight: .medium)
            }
            Spacer()
            Divider()
            Spacer()
            VStack(alignment: .leading){
                Text("Current Location:")
                    .customFont(size: 12, foregroundColor: AppColor.black, weight: .bold)
                Spacer()
                HStack{
                    NavigationLink(
                        destination: Text("\(providerDetails.providerLat) \(providerDetails.providerLong)").navigationBarHidden(false),
                        label: {
                            
                            Image("location_link")
                                .size(width: 15,height:15)
                            Text("View Map")
                                .customFont(size: 10, foregroundColor: AppColor.lightBlueText, weight: .medium)
                                .underline()
                        })
                }
            }
            Spacer()
        }
        .padding([.leading, .bottom, .trailing])
    }
}
struct BookingSlotPreviewView: View {
    var body: some View {
        VStack{
            HStack{
                HStack{
                    Text("Booking Time Slot")
                        .customFont(size: 14, foregroundColor: AppColor.black, weight: .bold)
                    Text("(1 Slot)")
                        .customFont(size: 10)
                }
                Spacer()
                Text("Edit").underline()
                    .customFont(size: 10, foregroundColor: AppColor.blackText)
                    
                    .onTapGesture {
                        print("Edit Time")
                    }
            }
            .padding(.top)
            Divider()
            HStack{
                Text("Date : ")
                    .customFont(size: 10, foregroundColor: AppColor.blackText)
                Text("24th May, 2021")
                    .customFont(size: 10, foregroundColor: AppColor.lightBlueText, weight: .bold)
                Spacer()
            }
            HStack{
                Text("Time : ")
                    .customFont(size: 10, foregroundColor: AppColor.blackText)
                Text("3:30 PM")
                    .customFont(size: 10, foregroundColor: AppColor.lightBlueText, weight: .bold)
                Spacer()
            }
            .padding(.bottom)
        }
        .padding(.horizontal)
        .background(Color.init(AppColor.lightBlueBackgroundColor))
        .cornerRadius(5)
    }
}

struct BookingDetailsView_Previews: PreviewProvider {
    static var previews: some View {
        BookingDetailsView()
    }
}

