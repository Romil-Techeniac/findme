//
//  BookingStatusDetails.swift
//  SwiftUI_Base
//
//  Created by Techeniac Services on 13/09/21.
//

import SwiftUI
import PopupView
//struct TrackkingStatusModel: Hashable, Identifiable {
//    let id = UUID()
//    var status: String
//    var time: String
//}
//
//struct trackingStatusData {
//    static var TrackingStatusList = [TrackkingStatusModel(status: "Your request has been accepted.", time: "04:30 PM, 19 June 2021"),
//                                     TrackkingStatusModel(status: "Your service provider is on the way.", time: "05:30 PM, 19 June 2021"),
//                                     TrackkingStatusModel(status: "Your service provider is arrived.", time: "7:10 PM, 19 June 2021"),
//                                     TrackkingStatusModel(status: "Work in progress.", time: "02:10 AM, 20 June 2021"),
//                                     TrackkingStatusModel(status: "Your work done.", time: "12:30 PM, 21 June 2021")]
//}
//
struct BookingStatusDetails: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @State var trackStatusCount : Int = 5
    @State var trackStatusDoneUpto : Int = 4
    
    @State var distanceKM: String = "12"
    @State var estimateTime: String = "05:30 PM"
    
    @State var bookingId: String = "0123456789"
    @State var providerImage: String = "work_1"
    @State var providerName: String = "Samir Sharma"
    @State var providerCategory: String = "Publumbing Searvice Carrier"
    @State var providerMessageId: String = "MSG_001"
    @State var providerMobileNumber: String = "9874562314"
    @State var isNavigation : Bool = true
    
    //    @Binding var trackStatusCount : Int
    //    let trackStatusCount = 4
    var body: some View {
        NavigationView{
            VStack{
                ScrollView{
                    VStack{
                        Spacer().frame(height: 19)
                        Group{
                            VStack{
                                // Distunce And Estimate View
                                DistanceAndEstimateView(distanceKM: $distanceKM, estimateTime: $estimateTime)
                            }
                            .padding([.leading,.trailing],16)
                            Spacer().frame(height: 15)
                            //status View
                            VStack{
                                ProviderBookingIdView(bookingId: $bookingId, providerImage: $providerImage, providerName: $providerName, providerCategory: $providerCategory, providerMessageId: $providerMessageId, providerMobileNumber: $providerMobileNumber)
                                Divider().padding(.horizontal)
                                Spacer().frame(height:15)
                                ScrollView {
                                    LazyVStack(spacing: 0) {
                                        ForEach(0..<trackingStatusData.TrackingStatusList.count, id: \.self) { trackingStatus in
                                            //                                            TrackStatusView(strStatus: "\(trackingStatus.[i])", strDate: "04:30 PM, 19 June 2021", imgIcon: trackingStatus+1 <= trackStatusDoneUpto ? "checkmark" : "time_circle", isLastRecord: i+1 == trackStatusCount, isStatusPassed: i+1 <= trackStatusDoneUpto)
                                            
                                            TrackStatusView(imgIcon: trackingStatus+1 <= trackStatusDoneUpto ? "checkmark" : "time_circle", isLastRecord: trackingStatus+1 == trackStatusCount, isStatusPassed: trackingStatus+1 <= trackStatusDoneUpto, trackingStatus: trackingStatusData.TrackingStatusList[trackingStatus])
                                                .listRowInsets(EdgeInsets())
                                        }
                                    }
                                }
                                .frame(height: CGFloat(trackStatusCount * 60))
                                //                                .background(Color.red)
                            }
                            .background(Color.white)
                            .cornerRadius(10)
                            .padding(.horizontal,16)
                        }
                        Spacer()
                    }
                }
                //Floting Button
                VStack{
                    Spacer()
                    NavigationLink(
//                        destination:VStack{ DashboardView().navigationBarHidden(true)},
                        destination:VStack{ RatingAndReviewPopup(dismiss: isNavigation).navigationBarHidden(true)},
                        isActive: $isNavigation){
                        HStack(alignment: .center){
                            Spacer()
                            Text("Go To Home")
                                .customFont(size: 17, foregroundColor: AppColor.white, weight: .bold)
                                .frame(height: 50)
                            Spacer()
                        }
                    }
                    .background(Color.init(AppColor.buttonBackgroundColor))
                    .cornerRadius(10)
                    .padding(.horizontal)
                    Spacer().frame( height: 5)
                }.frame( height: 50)
            }
            .background(Color.init(AppColor.AppBackgroundColor).ignoresSafeArea())
            .navigationBarTitleDisplayMode(.inline)            
            .navigationBarItems(leading:
                                    Button(action: {
                                        print("Back")
                                        self.presentationMode.wrappedValue.dismiss()
                                    }
                                    ){
                                        Image("back")
                                            .size(width: 30, height: 30, alignment: .center)
                                    }
            )
            .toolbar { // <2>
                ToolbarItem(placement: .principal) {
                    Text("View Booking Status")
                        .customFont(size: 16,
                                    foregroundColor: AppColor.black,
                                    weight: .bold)
                }
                
            }
        }
    }
}
struct TrackStatusView: View {
    @State var imgIcon = String()
    @State var isLastRecord : Bool = false
    //    @State var statusPassed : Color = .gray
    @State var isStatusPassed : Bool = false
    var trackingStatus : TrackingStatusModel
    var body: some View{
        HStack{
            Group{
                ZStack{
                    if !isLastRecord{
                        Line()
                            .stroke(style: StrokeStyle(lineWidth: 1, dash: [5]))
                            .foregroundColor(isStatusPassed ? Color.init(AppColor.lightBlueText) : Color.init(AppColor.subTitle))
                            .offset(x: 30)
                        //                    VStack(alignment: .leading){
                    }
                    HStack(alignment: .top){
                        VStack{
                            Image(imgIcon).size(width: 20, height: 20, alignment: .center)
                        }
                        .frame(width: 30, height: 30, alignment: .center)
                        .background( isStatusPassed ? Color.init(AppColor.lightBlueBackgroundColor) : Color.init(AppColor.lightGrayBackgroundColor))
                        .clipShape(Circle())
                        Spacer()
                    }
                    .offset(x:15,y:-15)
                    Spacer()
                        .frame(width: 50, height: 50)
                }
            }
            .frame(width: 50, height: 60, alignment: .center)
            Spacer().frame(width: 15)
            VStack(alignment: .leading){
                Text("\(trackingStatus.status)")
                    .customFont(size: 14, foregroundColor: isStatusPassed ? AppColor.trackstatusText : AppColor.subTitle)
                    .fixedSize(horizontal: false, vertical: true)
                Text("\(trackingStatus.time)")
                    .customFont(size: 10, foregroundColor: AppColor.subTitleGray)
                Spacer()
            }
            Spacer()
        }
    }
    struct Line: Shape {
        func path(in rect: CGRect) -> Path {
            var path = Path()
            path.move(to: CGPoint(x: 0, y: 0))
            path.addLine(to: CGPoint(x: 0, y: rect.height))
            return path
        }
    }
}

struct BookingStatusDetails_Previews: PreviewProvider {
    static var previews: some View {
        BookingStatusDetails()
        TrackStatusView(imgIcon: "checkmark", isLastRecord: false, isStatusPassed: true, trackingStatus: trackingStatusData.TrackingStatusList.first!)
    }
}

struct DistanceAndEstimateView: View {
    @Binding var distanceKM: String
    @Binding var estimateTime: String
    var body: some View {
        HStack{
            HStack{
                Spacer()
                VStack{
                    Image("location_1").size(width: 20, height: 20)
                }
                .frame(width: 35, height: 35, alignment: .center)
                .background(Color.init(AppColor.lightBlueBackgroundColor))
                .clipShape(Circle())
                VStack(alignment: .leading){
                    HStack{
                        Text("Distance:").customFont(size: 10, foregroundColor: AppColor.subTitle)
                        Text("\(distanceKM) KM").customFont(size: 10, foregroundColor: AppColor.subTitleGray, weight: .bold)
                        
                    }
                    NavigationLink(
                        destination: LocationView().navigationBarHidden(true),
                        label: {
                            
                            Text("View on map")
                                .underline()
                                .customFont(size: 14, foregroundColor: AppColor.lightBlueText)
                            //                                                .onTapGesture {
                            //                                                    print("View on map")
                            //                                                }
                        })
                }
                Spacer()
            }
            Divider().padding([.top,.bottom])
            HStack{
                VStack{
                    Image("estimate_time").size(width: 20, height: 20, alignment: .center)
                }
                .frame(width: 35, height: 35, alignment: .center)
                .background(Color.init(AppColor.lightBlueBackgroundColor))
                .clipShape(Circle())
                .padding(.leading,5)
                VStack(alignment: .leading){
                    Text("Est. Arrival Time")
                        .customFont(size: 10, foregroundColor: AppColor.subTitleGray, weight: .bold)
                    Text(estimateTime)
                        .customFont(size: 14, foregroundColor: AppColor.blackText, weight: .bold)
                }
                Spacer()
            }
        }.frame(height: 80, alignment: .center)
        .background(Color.white)
        .cornerRadius(10)
    }
}

struct ProviderBookingIdView: View {
    
    @Binding var bookingId: String
    @Binding var providerImage: String
    @Binding var providerName: String
    @Binding var providerCategory: String
    @Binding var providerMessageId: String
    @Binding var providerMobileNumber: String
    
    var body: some View {
        Group{
            HStack{
                Text("View status")
                    .customFont(size: 12, foregroundColor: AppColor.subTitle, weight: .bold)
                Spacer()
                Text("Booking ID: ")
                    .customFont(size: 12, foregroundColor: AppColor.subTitle, weight: .bold)
                Text("#\(bookingId)")
                    .customFont(size: 12, foregroundColor: AppColor.subTitle)
            }
            .padding([.horizontal,.top],16.0)
            Divider().padding(.horizontal)
            HStack{
                Image(providerImage).size(width: 50, height: 50, alignment: .center).cornerRadius(5)
                Spacer().frame(width: 10)
                VStack(alignment: .leading){
                    Spacer()
                    Text(providerName)
                        .customFont(size: 14, foregroundColor: AppColor.black, weight: .bold)
                    Spacer()
                    
                    Text(providerCategory)
                        .customFont(size: 10, foregroundColor: AppColor.subTitleGray)
                        .fixedSize(horizontal: false, vertical: true)
                    Spacer()
                    
                }
                Spacer()
                VStack(alignment: .trailing){
                    HStack{
                        Image("message")
                            .size(width: 40, height: 40, alignment: .center)
                            .cornerRadius(10)
                            .onTapGesture {
                                print(providerMessageId)
                            }
                        Image("call")
                            .size(width: 40, height: 40, alignment: .center)
                            .cornerRadius(10)
                            .onTapGesture {
                                print(providerMobileNumber)
                            }
                    }
                }
                .padding([.top,.bottom],10)
            }.frame(height: 30, alignment: .center)
            .padding(16.0)
            
        }
    }
}
