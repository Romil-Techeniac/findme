//
//  AddAddressView.swift
//  SwiftUI_Base
//
//  Created by Techeniac Services on 07/10/21.
//

import SwiftUI

struct AddAddressView: View {
    @Environment(\.presentationMode) var presentationMode : Binding<PresentationMode>
    
    @State var isNavigation : Bool = false
    
    @State var addressType : AddressType = .Home
    @State var addressHouseNo : String = ""
    @State var addressBuildingName : String = ""
    @State var addressLocality : String = ""
    @State var addressCity : String = ""
    @State var addressState : String = ""
    @State var addressPincode : String = ""
    @State var isSaveAddress : Bool = false
    
    //    var address : AddressModel
    //    init() {
    //
    //
    //
    //        addressType = address.type
    //        addressHouseNo = address.houseNo
    //        addressBuildingName = address.buildingName
    //        addressLocality = address.locality
    //        addressCity = address.city
    //        addressState = address.state
    //        addressPincode = address.pincode
    //    }
    
    var body: some View {
        NavigationView{
            VStack {
                ScrollView{
                    Spacer().frame( height: 19)
                    VStack(spacing: 20){
                        VStack{
                            VStack(alignment: .leading, spacing: 8.0){
                                HStack(spacing: 13){
                                    Text("Select Address Type")
                                        .customFont(size: 14, foregroundColor: AppColor.black, weight: .bold)
                                    Spacer()
                                    Menu {
                                        ForEach(AddressType.allCases, id: \.self) { type in
                                            Button("\(type.description)"){
                                                self.addressType = type
                                            }
                                        }
                                    } label: {
                                        HStack{
                                            Text("\(addressType.description)").customFont(size: 14, foregroundColor: AppColor.black, weight: .medium)
                                                .multilineTextAlignment(.trailing)
                                            Spacer()
                                            Image("ic_dropown").size(width: 20, height: 20)
                                        }
                                        .padding()
                                        .frame(width: 120, height: 50)
                                        .background(Color.init(AppColor.lightBlueBackgroundColor))
                                        .cornerRadius(5)
                                    }
                                    
                                }
                                Divider()
                            }
                            .padding(.top)
                            
                            VStack(alignment: .leading, spacing: 8.0){
                                Text("House/Block no.").customFont(size: 13, foregroundColor: AppColor.subTitle, weight: .regular)
                                TextField("Enter House/Block no.", text: .constant(addressHouseNo))
                                Divider()
                            }
                            .padding(.top)
                            
                            VStack(alignment: .leading, spacing: 8.0){
                                Text("Building/Society name").customFont(size: 13, foregroundColor: AppColor.subTitle, weight: .regular)
                                TextField("Enter Building/Society name", text: .constant(addressBuildingName))
                                Divider()
                            }
                            .padding(.top)
                            VStack(alignment: .leading, spacing: 8.0){
                                Text("Locality/Area").customFont(size: 13, foregroundColor: AppColor.subTitle, weight: .regular)
                                TextField("Enter Locality/Area", text: .constant(addressLocality))
                                Divider()
                            }
                            .padding(.top)
                            
                            VStack(alignment: .leading, spacing: 8.0){
                                Text("City").customFont(size: 13, foregroundColor: AppColor.subTitle, weight: .regular)
                                TextField("Enter City", text: .constant(addressCity))
                                Divider()
                            }
                            .padding(.top)
                            
                            VStack(alignment: .leading, spacing: 8.0){
                                Text("State").customFont(size: 13, foregroundColor: AppColor.subTitle, weight: .regular)
                                TextField("Enter State", text: .constant(addressState))
                                Divider()
                            }
                            .padding(.top)
                            
                            VStack(alignment: .leading, spacing: 8.0){
                                Text("Pincode").customFont(size: 13, foregroundColor: AppColor.subTitle, weight: .regular)
                                TextField("Enter Pincode", text: .constant(addressPincode))
                                    .keyboardType(.numberPad)
                                Divider()
                            }
                            .padding(.vertical)
                        }
                        .padding(.horizontal)
                        .background(Color.init(AppColor.white))
                        .cornerRadius(10)
                    }
                    .padding(.horizontal, 20.0)
                    HStack{
                        Button(action:
                                {
                                    self.isSaveAddress = !self.isSaveAddress
                                    print("State : \(self.isSaveAddress)")
                                }) {
                                Image(self.isSaveAddress ? "ic_checked" : "ic_radio_unchecked" )
                                    .frame(width:20, height:20, alignment: .center)
                        }
                        Text("Save this address details.")
                            .foregroundColor(Color.init(AppColor.subTitle))
                        Spacer()
                    }
                    .padding(.all, 20.0)

                }
                //Floting Button
                VStack{
                    Spacer()
//                    NavigationLink(
//                        destination:VStack{ Text("Go To Gome")},
//                        isActive: $isNavigation){
                        HStack(alignment: .center){
                            Spacer()
                            Text("Proceed")
                                .customFont(size: 17, foregroundColor: AppColor.white, weight: .bold)
                                .frame(height: 50)
                                .onTapGesture {
                                    let newAddress = AddressModel(type: addressType, houseNo: addressHouseNo, buildingName: addressBuildingName, locality: addressLocality, city: addressCity, state: addressState, pincode: addressPincode)
                                    
                                    AddressData.addressList.append(newAddress)
                                    print(isSaveAddress)
                                    
                                    self.presentationMode.wrappedValue.dismiss()
                                }
                            Spacer()
//                        }
                    }
                    .background(Color.init(AppColor.buttonBackgroundColor))
                    .cornerRadius(10)
                    .padding(.horizontal)
                    Spacer().frame( height: 5)
                }.frame( height: 50)
                
            }
            .navigationBarTitleDisplayMode(.inline)
            .navigationBarItems(leading:
                                    Button(action: {
                                        print("Back")
                                        self.presentationMode.wrappedValue.dismiss()
                                    }
                                    ){
                                        Image("back")
                                            .size(width: 30, height: 30, alignment: .center)
                                    }
            )
            .toolbar { // <2>
                ToolbarItem(placement: .principal) {
                    Text("Edit Address")
                        .customFont(size: 16,
                                    foregroundColor: AppColor.black,
                                    weight: .bold)
                }
            }
            .background(Color.init(AppColor.AppBackgroundColor).ignoresSafeArea())
            
            
        }.navigationViewStyle(StackNavigationViewStyle())

    }
}

struct AddAddressView_Previews: PreviewProvider {
    static var previews: some View {
        AddAddressView()
    }
}
