//
//  AddressListView.swift
//  SwiftUI_Base
//
//  Created by Techeniac Services on 07/10/21.
//

import SwiftUI

struct AddressListView: View {
    @State var isNavigation : Bool = false
    @Environment(\.presentationMode) var presentationMode : Binding<PresentationMode>
    @State var addressType : AddressType = .Home
    @State var selectedAddress : Bool = false
    @State private var selectedShortBy : shortBy? = .TopRated
    
    var addressList : [AddressModel]
    var addressListFiltered: [AddressModel] {
        switch addressType {
        case .Home:
            return addressList.filter { $0.type == .Home}
        case .Office:
            return addressList.filter { $0.type == .Office}
        case .Work:
            return addressList.filter { $0.type == .Work}
        }
    }
    
    var body: some View {
        NavigationView{
            VStack {
                ScrollView{
                    Spacer().frame( height: 19)
                    VStack(spacing: 20){
                        VStack(alignment: .leading, spacing: 8.0){
                            HStack(spacing: 13){
                                Text("Select Address Type")
                                    .customFont(size: 14, foregroundColor: AppColor.black, weight: .bold)
                                Spacer()
                                Menu {
                                    ForEach(AddressType.allCases, id: \.self) { type in
                                        Button("\(type.description)"){
                                            self.addressType = type
                                        }
                                    }
                                } label: {
                                    HStack{
                                        Text("\(addressType.description)").customFont(size: 14, foregroundColor: AppColor.black, weight: .medium)
                                            .multilineTextAlignment(.trailing)
                                        Spacer()
                                        Image("ic_dropown").size(width: 20, height: 20)
                                    }
                                    .padding()
                                    .frame(width: 120, height: 50)
                                    .background(Color.init(AppColor.white))
                                    .cornerRadius(5)
                                }
                            }
                            //                            Divider()
                        }
                        //                        .padding(.top)
                        ForEach(addressListFiltered, id:  \.self) { address in
                            AddressBlockView(address: address)
                            
                        }
                        if addressListFiltered.count < 3{
                            NavigationLink(
                                destination: AddAddressView().navigationBarHidden(true),
                                label: {
                                    Label("Add New Address", image: "ic_plus_white")
                                        .padding()
                                        .frame(height: 40.0)
                                        .font(.system(size: 14))
                                        .foregroundColor(Color.init(AppColor.white))
                                        .background(Color.init(AppColor.lightBlueText))
                                        .cornerRadius(5)
                                })
                            //                            .frame( height: 40)
                        }
                    }
                    .padding(.horizontal, 20.0)
                    
                }
                //Floting Button
                VStack{
                    Spacer()
                    NavigationLink(
                        destination:VStack{ BookingDetailsView().navigationBarHidden(true)},
                        isActive: $isNavigation){
                        HStack(alignment: .center){
                            Spacer()
                            Text("Next")
                                .customFont(size: 17, foregroundColor: AppColor.white, weight: .bold)
                                .frame(height: 50)
                            Spacer()
                        }
                    }
                    .background(Color.init(AppColor.buttonBackgroundColor))
                    .cornerRadius(10)
                    .padding(.horizontal)
                    Spacer().frame( height: 5)
                }.frame( height: 50)
                
            }
            .navigationBarTitleDisplayMode(.inline)
            .navigationBarItems(leading:
                                    Button(action: {
                                        print("Back")
                                        self.presentationMode.wrappedValue.dismiss()
                                    }
                                    ){
                                        Image("back")
                                            .size(width: 30, height: 30, alignment: .center)
                                    }
            )
            .toolbar { // <2>
                ToolbarItem(placement: .principal) {
                    Text("Select Address")
                        .customFont(size: 16,
                                    foregroundColor: AppColor.black,
                                    weight: .bold)
                }
            }
            .background(Color.init(AppColor.AppBackgroundColor).ignoresSafeArea())
            
            
        }.navigationViewStyle(StackNavigationViewStyle())
    }
}

struct AddressList_Previews: PreviewProvider {
    static var previews: some View {
        AddressListView(addressList: AddressData.addressList)
    }
}

struct AddressBlockView: View {
    var address : AddressModel
    //    @Binding var selectedAddress : Bool
    
    var body: some View {
        HStack(alignment: .center){
            Button(action:
                    {
                        //                        address.isSelected = true
                        //                        print("State : \(address.isSelected.toggle())")
                        
                    }){
                HStack{
                    
                    Image(self.address.isSelected ? "ic_radio_checked" : "ic_radio_unchecked" )
                        .frame(width:20, height:20, alignment: .center)
                    Text("\(address.houseNo), \(address.buildingName), \(address.locality), \(address.city), \(address.state), \(address.pincode).")
                        .customFont(size: 12, foregroundColor: AppColor.black, weight: .regular)
                        .frame(maxWidth : 200)
                    //                                        .multilineTextAlignment(.center)
                }
            }
            Spacer()
            //            Button(action:
            //                    {
            //
            ////                        self.selectedAddress = !self.selectedAddress
            //                        print("Edit : \(self.address.buildingName)")
            //                    }) {
            NavigationLink(
                destination: AddAddressView(addressType: address.type, addressHouseNo: address.houseNo, addressBuildingName: address.buildingName, addressLocality: address.locality, addressCity: address.city, addressState: address.state, addressPincode: address.pincode, isSaveAddress: true).navigationBarHidden(true),
                label: {
                    
                    HStack{
                        Text("Edit")
                            .customFont(size: 12, foregroundColor: AppColor.lightBlueText , weight: .regular)
                            .underline()
                    }
                })
            Spacer()
            Button(action:
                    {
                        //                        self.selectedAddress = !self.selectedAddress
                        print("State : \(address.isSelected)")
                        AddressData.addressList.remove(at: AddressData.addressList.firstIndex(of: address)!)
                    }) {
                HStack{
                    Text("Delete")
                        .customFont(size: 12, foregroundColor: AppColor.lightRedText , weight: .regular)
                        .underline()
                }
            }
        }
        .padding(.all)
        .background(Color.init(AppColor.white))
        .cornerRadius(10)
    }
}
