//
//  NotificationView.swift
//  SwiftUI_Base
//
//  Created by Techeniac Services on 19/10/21.
//

import SwiftUI

struct NotificationView: View {
    @Environment(\.presentationMode) var presentationMode : Binding<PresentationMode>
    @State var isNavigation : Bool = false
    
    var notificationList : [NotificationModel] = NotificationData.NotificationList
    
    var body: some View {
        NavigationView{
            VStack(spacing: 20.0){
                
                ScrollView{
                    ForEach(notificationList, id: \.id) { notification in
                        
                        NavigationLink(
                            destination: Text(notification.category),
                            isActive: $isNavigation){
                            NotificationCell(notification: notification)
                        }
                    }
                    //                    NotificationCell(notification: notificationList[0])
                }
                
            }.padding(.all, 20.0)
            
            .navigationBarTitleDisplayMode(.inline)
            .navigationBarItems(leading:
                                    Button(action: {
                                        print("Back")
                                        self.presentationMode.wrappedValue.dismiss()
                                    }
                                    ){
                                        Image("back")
                                            .size(width: 30, height: 30, alignment: .center)
                                    }
            )
            .toolbar { // <2>
                ToolbarItem(placement: .principal) {
                    Text("Notification")
                        .customFont(size: 16,
                                    foregroundColor: AppColor.black,
                                    weight: .bold)
                }
            }
            .background(Color.init(AppColor.AppBackgroundColor).ignoresSafeArea())
        }
    }
    
}

struct NotificationView_Previews: PreviewProvider {
    static var previews: some View {
        NotificationView()
    }
}

struct NotificationCell: View {
    var notification : NotificationModel
    var body: some View {
        HStack{
            ZStack{
                HStack{
                    Image(notification.category).size(width: 16, height: 16)
                        .padding(8.0)
                }
                .frame(width: 40, height: 40)
                .background(Color.init(AppColor.lightBlueBackgroundColor))
                .cornerRadius(20)
                if !notification.isReaded{
                    Image("ic_notification_unread")
                        .size(width: 9, height: 9)
                        .offset(x: 15, y: -15)
                }
            }
            VStack(alignment: .leading){
                Text(notification.title).customFont(size: 12, foregroundColor: AppColor.black, weight: .medium)
                    .fixedSize(horizontal: false, vertical: true)
                Text(getNotificationTime(date: notification.date)).customFont(size: 10, foregroundColor: AppColor.subTitleGray, weight: .medium)
                
            }
        }
        .padding(14.0)
        .frame(height: 70)
        .background(Color.init(AppColor.white))
        .cornerRadius(8)
    }
//    func getNotificationTime(date : Date) -> String {
//        let day = date.days(from: Date())
//        let hour = date.hours(from: Date())
//        let minutes = date.minutes(from: Date())
//        print(day,hour,minutes)
//        if day < 7{
//            if day <= 1{
//                if hour <= 1 {
//                    return minutes <= 1 ?  "just Now" : "\(minutes) minutes ago"
//                }else{
//                    return hour <= 1 ?  "\(hour) minute ago" : "\(hour) minutes ago"
//                }
//            }else{
//                return day <= 1 ? "\(day) day ago" : "\(day) days ago"
//            }
//        }else{
//            return date.formatDate(format: "dd/MM/yyyy")
//        }
//    }
}
public func getNotificationTime(date : Date) -> String {
    let day = date.days(from: Date())
    let hour = date.hours(from: Date())
    let minutes = date.minutes(from: Date())
    print(day,hour,minutes)
    if day < 7{
        if day <= 1{
            if hour <= 1 {
                return minutes <= 1 ?  "just Now" : "\(minutes) minutes ago"
            }else{
                return hour <= 1 ?  "\(hour) minute ago" : "\(hour) minutes ago"
            }
        }else{
            return day <= 1 ? "\(day) day ago" : "\(day) days ago"
        }
    }else{
        return date.formatDate(format: "dd/MM/yyyy")
    }
}
extension Date {
    /// Returns the amount of years from another date
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    /// Returns the amount of weeks from another date
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfMonth], from: date, to: self).weekOfMonth ?? 0
    }
    /// Returns the amount of days from another date
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    /// Returns the amount of seconds from another date
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    /// Returns the a custom time interval description from another date
    func offset(from date: Date) -> String {
        if years(from: date)   > 0 { return "\(years(from: date))y"   }
        if months(from: date)  > 0 { return "\(months(from: date))M"  }
        if weeks(from: date)   > 0 { return "\(weeks(from: date))w"   }
        if days(from: date)    > 0 { return "\(days(from: date))d"    }
        if hours(from: date)   > 0 { return "\(hours(from: date))h"   }
        if minutes(from: date) > 0 { return "\(minutes(from: date))m" }
        if seconds(from: date) > 0 { return "\(seconds(from: date))s" }
        return ""
    }
}
