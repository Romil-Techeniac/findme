//
//  ProviderProfileDetailView.swift
//  SwiftUI_Base
//
//  Created by Techeniac Services on 05/10/21.
//

import SwiftUI

struct ProviderProfileDetailView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @State var id : String = ""
    @State var isNavigation : Bool = false
    var providerDetails : ProviderModel = ProviderData.ProviderList[0]
    
    var body: some View {
        NavigationView{
            VStack{
                ScrollView{
                    VStack{
                        Spacer().frame(height: 19)
                        Group{
                            VStack{
                                //ServiceProvider View
                                ProviderDetailPreviewView(providerDetails: providerDetails)
                            }
                        }
                    }
                }
                .edgesIgnoringSafeArea(.bottom)
                
                //Floting Button
                VStack{
                    Spacer()
                    
                    HStack(spacing: 15.0){
                        NavigationLink(
                            destination:VStack{ AddressListView(addressList: AddressData.addressList).navigationBarHidden(true)},
                            isActive: $isNavigation){
                            HStack(alignment: .center){
                                Spacer()
                                Text("Book Now")
                                    .customFont(size: 17, foregroundColor: AppColor.white, weight: .bold)
                                    .frame(height: 50)
                                Spacer()
                            }
                        }
                        .background(Color.init(AppColor.buttonBackgroundColor))
                        .cornerRadius(10)
                        .padding(.horizontal)
                        
                        NavigationLink(
                            destination:VStack{ BookingSlotView().navigationBarHidden(true)},
                            isActive: $isNavigation){
                            HStack(alignment: .center){
                                Spacer()
                                Text("Book Later")
                                    .customFont(size: 17, foregroundColor: AppColor.white, weight: .bold)
                                    .frame(height: 50)
                                Spacer()
                            }
                        }
                        .background(Color.init(AppColor.buttonBackgroundColor))
                        .cornerRadius(10)
                        .padding(.trailing)
                        
                    }
                    //                    NavigationLink(
                    //                        destination:VStack{ Text("Destination")},
                    //                        isActive: $isNavigation){
                    //                        HStack(alignment: .center){
                    //                            Spacer()
                    //                            Text("Proceed to CheckOut")
                    //                                .customFont(size: 17, foregroundColor: AppColor.white, weight: .bold)
                    //                                .frame(height: 50)
                    //                            Spacer()
                    //                        }
                    //                    }
                    //                    .background(Color.init(AppColor.buttonBackgroundColor))
                    //                    .cornerRadius(10)
                    //                    .padding(.horizontal)
                    Spacer().frame( height: 5)
                }.frame(height: 50)
            }
            .navigationBarTitleDisplayMode(.inline)
            .navigationBarItems(leading:
                                    Button(action: {
                                        print("Back")
                                        self.presentationMode.wrappedValue.dismiss()
                                    }
                                    ){
                                        Image("back")
                                            .size(width: 30, height: 30, alignment: .center)
                                    }
            )
            .toolbar { // <2>
                ToolbarItem(placement: .principal) {
                    Text("Profile Details")
                        .customFont(size: 16,
                                    foregroundColor: AppColor.black,
                                    weight: .bold)
                }
            }
            .background(Color.init(AppColor.AppBackgroundColor).ignoresSafeArea())
        }.onAppear{
            print(id)
        }
    }
}

struct ProviderProfileDetailView_Previews: PreviewProvider {
    static var previews: some View {
        ProviderProfileDetailView()
    }
}

struct ProviderDetailPreviewView: View {
    var providerDetails : ProviderModel
    
    var body: some View {
        Group{
            VStack{
                //Service Provier Profile
                ServiceProviderFullProfileView(providerDetails: providerDetails)
                Divider().padding(.horizontal)
                //About Excellent Care
                AboutExcellentCareView(providerDetails: providerDetails)
                Divider().padding(.horizontal)
                //Service Provier Ratio
                ProviderRateView(providerDetails: providerDetails)
                Divider().padding(.horizontal)
                //Availability
                HStack{
                    VStack(alignment: .leading){
                        Spacer()
                        
                        Text("Availability")
                            .customFont(size: 12, foregroundColor: AppColor.black, weight: .bold)
                        Spacer()
                        Text("Availability As Needed  (Mon to Sat)")
                            .customFont(size: 10, foregroundColor: AppColor.subTitleGray)
                            .fixedSize(horizontal: false, vertical: true)
                        Spacer()
                        
                        Text("9:00 AM to 5:00 PM")
                            .customFont(size: 10, foregroundColor: AppColor.subTitleDark, weight: .bold)
                        Spacer()
                        
                        
                    }
                    Spacer()
                    VStack(alignment: .trailing){
                        HStack{
                            Image("message")
                                .size(width: 40, height: 40, alignment: .center)
                                .cornerRadius(10)
                                .onTapGesture {
                                    print("providerMessageId")
                                }
                            Image("call")
                                .size(width: 40, height: 40, alignment: .center)
                                .cornerRadius(10)
                                .onTapGesture {
                                    print(providerDetails.providerContactNumber)
                                }
                        }
                    }
                    .padding([.top,.bottom],10)
                }.frame(height: 30
                        , alignment: .center)
                .padding(16.0)
                
                
                Divider().padding(.horizontal)
                ProviderContactDetailsView(providerDetails: providerDetails)
            }
            .background(Color.white)
            .cornerRadius(10)
        }
        .padding(.horizontal,16)
        .frame(minHeight: 50, maxHeight: .infinity)
    }
}

struct AboutExcellentCareView: View {
    var providerDetails : ProviderModel
    
    var body: some View {
        VStack(alignment: .leading, spacing: 5.0){
            Text("About Excellent Care")
                .customFont(size: 14, foregroundColor: AppColor.black, weight: .bold)
            Text(providerDetails.providerAboutCare)
                .customFont(size: 10, foregroundColor: AppColor.subTitleGray, weight: .medium)
                .fixedSize(horizontal: false, vertical: true)
            
        }.padding(.horizontal)
    }
}
