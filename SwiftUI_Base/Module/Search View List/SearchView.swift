//
//  SearchView.swift
//  SwiftUI_Base
//
//  Created by Techeniac Services on 01/10/21.
//

import SwiftUI

struct SearchView: View {
    @Environment(\.presentationMode) var presentationMode : Binding<PresentationMode>
    @State private var SearchListCellSize: [GridItem] = [GridItem(.flexible())]
    @State var isNavigation : Bool = false
    
    var body: some View {
        NavigationView{
            ScrollView{
                LazyVGrid(columns: SearchListCellSize,spacing: 10) {
                    ForEach(searchData.searchList, id: \.id) { search in
                        NavigationLink(
                            destination: ProviderProfileDetailView().navigationBarHidden(true),
                            isActive: $isNavigation){
                            ProviderSmallView(nearByList: search)
                        }
                    }
                }
                Spacer()
            }.padding(.all, 20.0)
            .navigationBarTitleDisplayMode(.inline)
            .navigationBarItems(leading:
                                    Button(action: {
                                        print("Back")
                                        self.presentationMode.wrappedValue.dismiss()
                                    }
                                    ){
                                        Image("back")
                                            .size(width: 30, height: 30, alignment: .center)
                                    }
                                , trailing:
                                    Button(action: {
                                        print("ic_setting")
//                                        self.presentationMode.wrappedValue.dismiss()
                                    }
                                    ){
                                        Image("ic_setting")
                                            .size(width: 30, height: 30, alignment: .center)
                                    }
            )
            .toolbar { // <2>
                ToolbarItem(placement: .principal) {
                    Text("\(searchData.searchList.count) Plumbers")
                        .customFont(size: 16,
                                    foregroundColor: AppColor.black,
                                    weight: .bold)
                }
            }
            .background(Color.init(AppColor.AppBackgroundColor).ignoresSafeArea())
//                        .background(Color.init(AppColor.black).ignoresSafeArea())
        }    }
}

struct SearchView_Previews: PreviewProvider {
    static var previews: some View {
        SearchView()
    }
}
