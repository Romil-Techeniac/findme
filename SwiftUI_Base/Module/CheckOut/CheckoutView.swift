//
//  CheckoutView.swift
//  SwiftUI_Base
//
//  Created by Techeniac Services on 13/10/21.
//

import SwiftUI
enum PaymentType{
    case Wallet
    case Card
    case GPay
    case PayPal
    case Cash
    var description : String{
        switch self {
        case .Wallet: return "Use Wallet Amount"
        case .Card: return "Credit / Debit Card"
        case .GPay: return "Google Pay"
        case .PayPal: return "Paypal Payment"
        case .Cash: return "Cash on Delivery"
        }
    }
}

struct CheckoutView: View {
    @Environment(\.presentationMode) var presentationMode : Binding<PresentationMode>
    @State private var isNavigation : Bool = false
    //    @State private var txtLocation : String = ""
    //    @State private var txtMinPrice : String = "0"
    //    @State private var txtMaxPrice : String = "100"
    @State private var selectedPaymentType : PaymentType? = .Wallet
    @State private var showingCardSheet : Bool = false
    @State private var showingSuccsessSheet : Bool = false
    @State private var selectedCard : Bool = false
    @State private var isCardExpand : Bool = false
    @State private var PaymentTypeList : [PaymentType] = [.Wallet, .Card, .GPay, .Cash, .PayPal]
    @State private var CardList : [CardModel] = CardData.CardList
    //    @State private var selectedServiceList : [String] = []
    
    
    var body: some View {
        NavigationView{
            VStack{
                ScrollView{
                    VStack(spacing: 30.0){
                        VStack(alignment: .leading){
                            VStack(spacing: 17) {
                                PaymentCellView(SelectedPayment: $selectedPaymentType, payment: .Wallet)
                                VStack{
                                    VStack{
                                        HStack{
                                            Image("\(PaymentType.Card)")
                                            Text("\(PaymentType.Card.description)").customFont(size: 14, foregroundColor: selectedPaymentType == PaymentType.Card ? AppColor.blackText : AppColor.subTitle , weight: .medium)
                                            Spacer()
                                            Image("ic_dropown")
                                                .size(width: 22, height: 22)
                                                .rotationEffect(self.selectedPaymentType == PaymentType.Card ? .degrees(0) : .degrees(-90))
                                        }
                                    }
                                    .padding(.horizontal)
                                    .frame(height: 50.0)
                                    //                                    .background(Color.init(selectedPaymentType == PaymentType.Card ? AppColor.lightBlueBackgroundColor : AppColor.white))
                                    .cornerRadius(10)
                                    .onTapGesture {
                                        selectedPaymentType = PaymentType.Card
                                    }
                                    if selectedPaymentType == PaymentType.Card{
                                        ForEach(CardList, id: \.self) { card in
                                            VStack {
                                                Divider()
                                                    .padding(.leading)
                                                HStack(spacing: 1.0){
                                                    VStack(alignment: .leading) {
                                                        Text(card.name).customFont(size: 14, foregroundColor: card.isSelected ? AppColor.blackText : AppColor.subTitleGray , weight: .medium)
                                                        
                                                        Text(card.number).customFont(size: 12, foregroundColor: AppColor.subTitleGray , weight: .medium)
                                                    }
                                                    Spacer()
                                                    Image(card.isSelected == true ? "ic_radio_checked" : "ic_radio_unchecked")
                                                }
                                                .padding(.horizontal)
                                            }.onTapGesture {
                                                print(card.name)
                                            }
                                        }
                                        if CardList.count < 3{
                                            //                                        NavigationLink(
                                            //                                            destination: AddAddressView().navigationBarHidden(true),
                                            //                                            label: {
                                            Button(action: {
                                                showingCardSheet.toggle()
                                            }) {
                                                
                                                Label("Add New Card", image: "ic_plus_blue")
                                                    .padding()
                                                    .frame(height: 40.0)
                                                    .font(.system(size: 14))
                                                    .foregroundColor(Color.init(AppColor.lightBlueText))
                                                    .background(Color.init(AppColor.lightBlueBackgroundColor))
                                                    .cornerRadius(5)
                                                    //                                            })
                                                    .sheet(isPresented: $showingCardSheet) {
                                                        AddCardView(dismiss: $showingCardSheet)
                                                    }
                                            }
                                            
                                            .padding(.top)
                                        }
                                    }
                                }
                                .padding(.bottom, selectedPaymentType == PaymentType.Card ? 14 : 0)
                                .overlay(RoundedRectangle(cornerRadius: 10).stroke(lineWidth: 2).foregroundColor(Color.init(AppColor.borderColor).opacity(0.2)))
                                .background(Color.init(selectedPaymentType == PaymentType.Card ? AppColor.white : AppColor.white))
                                
                                PaymentCellView(SelectedPayment: $selectedPaymentType, payment: .PayPal)
                                PaymentCellView(SelectedPayment: $selectedPaymentType, payment: .Cash)
                            }.padding(21)
                        }
                    }
                }
                .edgesIgnoringSafeArea(.bottom)
                // Note
                HStack{
                    Text("Note:").customFont(size: 14, foregroundColor: AppColor.lightBlueText, weight: .regular)
                    Text("This charges are consider as a visiting fees.").customFont(size: 14, foregroundColor: AppColor.subTitleGray, weight: .regular)
                }.padding(.vertical, 8.0)
                
                //Floting Button
                VStack{
                    Spacer()
                    HStack(alignment: .center){
                        Spacer()
                        Button(action: {
                            print("Pay Now")
                            showingSuccsessSheet.toggle()
                        }, label: {
                            HStack(alignment: .bottom, spacing: 1.0) {
                                Text("Pay now")
                                    .customFont(size: 17, foregroundColor: AppColor.white, weight: .bold)
                                Spacer()
                                Text("$40.00")
                                    .customFont(size: 17, foregroundColor: AppColor.white, weight: .bold)
                                Text("(Visiting Fees)")
                                    .customFont(size: 10, foregroundColor:  "80"+AppColor.white, weight: .regular)
                                
                            }.padding(.horizontal, 14)
                            .sheet(isPresented: $showingSuccsessSheet, content: {
                                paymentSuccessView(dismiss: $showingSuccsessSheet)
                            })
                        })
                        .frame(height: 50)
                        Spacer()
                    }
                    //                    .background(Color(red: 5/255, green: 30/255, blue: 49/255))
                    .background(Color.init(AppColor.buttonBackgroundColor))
                    //
                    .cornerRadius(10)
                    .padding(.horizontal)
                    Spacer().frame( height: 5)
                }.frame(height: 50)
            }
            .background(Color.init(AppColor.AppBackgroundColor).ignoresSafeArea())
            .navigationBarTitleDisplayMode(.inline)
            .navigationBarItems(leading:
                                    Button(action: {
                                        print("Back")
                                        self.presentationMode.wrappedValue.dismiss()
                                    }
                                    ){
                                        Image("back")
                                            .size(width: 30, height: 30, alignment: .center)
                                    }
            )
            .toolbar { // <2>
                ToolbarItem(placement: .principal) {
                    Text("Checkout")
                }
            }
            
        }
    }
}

struct CheckoutView_Previews: PreviewProvider {
    static var previews: some View {
        CheckoutView()
    }
}
struct PaymentCellView: View {
    
    @Binding var SelectedPayment: PaymentType?
    var payment: PaymentType
    
    var body: some View {
        VStack(spacing: 0.0) {
            HStack{
                Button(action: {
                    self.SelectedPayment = self.payment
                }) {
                    HStack(spacing: 10.0){
                        Image("\(payment)")
                        
                        Text("\(self.payment.description)").customFont(size: 14, foregroundColor: SelectedPayment == payment ? AppColor.blackText : AppColor.subTitle , weight: .medium)
                        Spacer()
                        Image(self.SelectedPayment == self.payment ? "ic_radio_checked" : "ic_radio_unchecked")
                    }
                }
            }
            .padding(.horizontal)
            .frame(height: 50.0)
            .background(Color.init(self.SelectedPayment == self.payment ? AppColor.lightBlueBackgroundColor : AppColor.white))
            .cornerRadius(10)
            //            Divider().padding(.horizontal)
        }
        .overlay(RoundedRectangle(cornerRadius: 10).stroke(lineWidth: 2).foregroundColor(Color.init(AppColor.borderColor).opacity(0.2)))
    }
}

struct AddCardView: View {
    @Binding var dismiss : Bool
    @State private var txtCardNumber = String()
    @State private var txtCardName = String()
    @State private var txtCardCvv = String()
    @State private var txtCardExpiry = String()
    @State private var isSaveAddress : Bool = false
    
    var body: some View {
        VStack(alignment: .leading){
            Spacer()
            Divider().frame(width: 60, height: 5, alignment: .center)
            // Card Name
            Group{
                Text("Card Name")
                    .customFont(size: 14, foregroundColor: AppColor.title)
                Group{
                    TextField("Enter name as per card", text: $txtCardName)
                        .background(Color.init(AppColor.textfeildBackground))
                        .foregroundColor(.black)
                        .frame( height: 50)
                    //                        .keyboardType(.numberPad)
                }
                .padding(.horizontal)
                .background(Color.init(AppColor.textfeildBackground))
                .frame( height: 50)
                .cornerRadius(10)
                .overlay(RoundedRectangle(cornerRadius: 10).stroke(lineWidth: 2).foregroundColor(Color.init(AppColor.borderColor).opacity(0.2)))
            }
            
            // Card Number
            Group{
                Text("Card Number")
                    .customFont(size: 14, foregroundColor: AppColor.title)
                Group{
                    TextField("XXXX    XXXX    XXXX    4562", text: $txtCardNumber)
                        .background(Color.init(AppColor.textfeildBackground))
                        .foregroundColor(.black)
                        .frame( height: 50)
                        .keyboardType(.numberPad)
                }
                .padding(.horizontal)
                .background(Color.init(AppColor.textfeildBackground))
                .frame( height: 50)
                .cornerRadius(10)
                .overlay(RoundedRectangle(cornerRadius: 10).stroke(lineWidth: 2).foregroundColor(Color.init(AppColor.borderColor).opacity(0.2)))
            }
            // Cvv And Expiry
            HStack{
                VStack(alignment: .leading){
                    Text("Cvv")
                        .customFont(size: 14, foregroundColor: AppColor.title)
                    Group{
                        TextField("eg. 123 ", text: $txtCardCvv)
                            .background(Color.init(AppColor.textfeildBackground))
                            .frame( height: 50)
                            .foregroundColor(.black)
                            .keyboardType(.numberPad)
                    }
                    .padding(.horizontal)
                    .background(Color.init(AppColor.textfeildBackground))
                    .frame( height: 50)
                    .cornerRadius(10)
                    .overlay(RoundedRectangle(cornerRadius: 10).stroke(lineWidth: 2).foregroundColor(Color.init(AppColor.borderColor).opacity(0.2)))
                }
                VStack(alignment: .leading){
                    Text("Expiry Date")
                        .customFont(size: 14, foregroundColor: AppColor.title)
                    Group{
                        TextField("Month/Year", text: $txtCardExpiry)
                            .background(Color.init(AppColor.textfeildBackground))
                            .foregroundColor(.black)
                            .frame( height: 50)
                            .keyboardType(.numberPad)
                    }
                    .padding(.horizontal)
                    .background(Color.init(AppColor.textfeildBackground))
                    .frame( height: 50)
                    .cornerRadius(10)
                    .overlay(RoundedRectangle(cornerRadius: 10).stroke(lineWidth: 2).foregroundColor(Color.init(AppColor.borderColor).opacity(0.2)))
                }
                
            }
            HStack{
                Button(action:
                        {
                            self.isSaveAddress = !self.isSaveAddress
                            print("State : \(self.isSaveAddress)")
                        }) {
                    Image(self.isSaveAddress ? "ic_checked" : "ic_radio_unchecked" )
                        .frame(width:20, height:20, alignment: .center)
                }
                Text("Save this card details.")
                    .foregroundColor(Color.init(AppColor.subTitle))
                Spacer()
            }
            .padding(.vertical, 20.0)
            
            //Floting Button
            VStack{
                Spacer()
                HStack(alignment: .center){
                    Spacer()
                    Button(action: {
                        print("Add Card")
                        dismiss.toggle()
                    }, label: {
                        HStack(alignment: .bottom, spacing: 1.0) {
                            Text("Pay now")
                                .customFont(size: 17, foregroundColor: AppColor.white, weight: .bold)
                            Spacer()
                            Text("$40.00")
                                .customFont(size: 17, foregroundColor: AppColor.white, weight: .bold)
                            Text("(Visiting Fees)")
                                .customFont(size: 10, foregroundColor:  "80"+AppColor.white, weight: .regular)
                            
                        }.padding(.horizontal, 14)
                        
                    })
                    .frame(height: 50)
                    
                    Spacer()
                }
                //                    .background(Color(red: 5/255, green: 30/255, blue: 49/255))
                .background(Color.init(AppColor.buttonBackgroundColor))
                //
                .cornerRadius(10)
                Spacer().frame( height: 5)
            }.frame(height: 50)
            // Note
            HStack{
                Text("Note:").customFont(size: 14, foregroundColor: AppColor.lightBlueText, weight: .regular)
                
                Text("This charges are consider as a visiting fees.").customFont(size: 14, foregroundColor: AppColor.subTitleGray, weight: .regular)
            }.padding(.top, 8.0)
        }.padding(20.0)
    }
}

//struct paymentSuccessView: View {
//    @Binding var dismiss : Bool
//    
//    var body: some View {
//        NavigationView{
//        VStack{
//            Spacer()
//            Image("ic_success_payment").size(width: 112, height: 112)
////            Image("location_1").size(width: 112, height: 112)
//            Spacer().frame(height: 16.0)
//            Text("Payment Success").customFont(size: 20, foregroundColor: AppColor.black, weight: .medium)
//            Spacer().frame(height: 6.0)
//            
//            Text("Your payment has been confirmed.").customFont(size: 14, foregroundColor: AppColor.subTitle, weight: .regular)
//            //Floting Button
//            Spacer().frame(height: 30.0)
//            VStack{
//                Spacer()
//                HStack(alignment: .center){
//                    Spacer()
//                    NavigationLink(
//                        destination: BookingStatusDetails().navigationBarHidden(true),
//                        label: {
//                            HStack(alignment: .bottom, spacing: 1.0) {
//                                Text("View Booking Status")
//                                    .customFont(size: 17, foregroundColor: AppColor.white, weight: .bold)
//                            }.padding(.horizontal, 14)
//                            
//                        })
//                        .frame(height: 50)
//                    
//                    Spacer()
//                }
//                //                    .background(Color(red: 5/255, green: 30/255, blue: 49/255))
//                .background(Color.init(AppColor.buttonBackgroundColor))
//                //
//                .cornerRadius(10)
//                Spacer().frame( height: 5)
//            }.frame(height: 50)
//        }
//        .padding(.horizontal, 20.0)
//    }
//    }
//    
//}
