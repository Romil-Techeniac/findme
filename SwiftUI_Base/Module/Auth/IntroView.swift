//
//  IntroView.swift
//  SwiftUI_Base
//
//  Created by Techeniac Services on 29/09/21.
//

import SwiftUI

struct IntroView: View {
    
    @State var txtName : String = ""
    @State var txtLocation : String
    @State var isNavigation : Bool = false
    
    var body: some View {
        NavigationView{
            ScrollView{
                VStack(alignment: .leading){
                    Image( "app_logo").size(width: 100, height: 35, alignment: .center)
                    Spacer().frame(height:35)
                    Group{
                        Text("Enter your name")
                            .customFont(size: 28, foregroundColor: AppColor.headline, weight: .bold)
                        Text("Please enter name and location for better experience.")
                            .customFont(size: 12, foregroundColor: AppColor.title)
                    }
                    Spacer().frame(height:35)
                    Group{
                        // Enter Your Name
                        Text("Enter Your Name")
                            .customFont(size: 14, foregroundColor: AppColor.title)
                        Group{
                            TextField("Enter Name", text: $txtName)
                                .background(Color.white)
                                .frame( height: 50)
                                .autocapitalization(.words)
                            //                                .keyboardType(.numberPad)
                        }
                        .padding(.horizontal)
                        .background(Color.init(AppColor.white))
                        .frame( height: 50)
                        .cornerRadius(10)
                        .overlay(RoundedRectangle(cornerRadius: 10).stroke(lineWidth: 2).foregroundColor(Color.init(AppColor.borderColor).opacity(0.2)))
                        
                        // Enter Location
                        
                        Group{
                            Text("Enter Location")
                                .customFont(size: 14, foregroundColor: AppColor.title)
                            Group{
                                TextField("Enter Your Location", text: $txtLocation)
//                                TextField("Enter Your Location", text: .constant("\(123)"))
                                    .background(Color.white)
                                    .frame( height: 50)
                                //                                    .keyboardType(.numberPad)
                            }
                            .padding(.horizontal)
                            .background(Color.init(AppColor.white))
                            .frame( height: 50)
                            .cornerRadius(10)
                            .overlay(RoundedRectangle(cornerRadius: 10).stroke(lineWidth: 2).foregroundColor(Color.init(AppColor.borderColor).opacity(0.2)))
                            
                        }
                    }
                    Spacer().frame(height:12)
                    NavigationLink(
                        destination: LocationView().navigationBarHidden(true),
                        label: {
                            HStack{
                                Image("location")
                                    .size(width: 16, height: 16, alignment: .center)
                                Text("Use Current Location Instead")
                                    .customFont(size: 14, foregroundColor: AppColor.blackText, weight: .medium)
                                    .underline()
                                    .padding(1.0)
                                
                            }
                        })
                    Spacer().frame(height:30)
                    
                    Group{
                        NavigationLink(destination:
                                        VStack{
                                            TabBarView(viewRouter: ViewRouter()).navigationBarHidden(true)
                                        }
                                       , isActive: $isNavigation) { // open ProjectsView only if the user clicked on the item "Projects" of the list etc..
                            HStack(alignment: .center){
                                Spacer()
                                Text("Submit")
                                    .customFont(size: 17, foregroundColor: AppColor.white, weight: .bold)
                                    .frame(height: 50)
                                    .onTapGesture {
                                        isNavigation = true
                                    }
                                Spacer()
                            }
                            .background(isvalid() ? Color.init(AppColor.buttonBackgroundColor) : Color.init(AppColor.lightGrayBackgroundColor))
                            .cornerRadius(10)
                        }.disabled(!isvalid())
                    }
                }
            }
            .padding(.all, 38)
            .background(Color.init(AppColor.AppBackgroundColor).ignoresSafeArea())
            .navigationBarHidden(true)
        }
        
    }
    func isvalid() -> Bool{
        print("Name And Location", txtName, txtLocation)
        return (!txtName.isEmpty && !txtLocation.isEmpty) ? true : false
    }
}

struct IntroView_Previews: PreviewProvider {
    static var previews: some View {
        IntroView(txtLocation: "surat")
    }
}
