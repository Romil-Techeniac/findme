//
//  LoginView.swift
//  SwiftUI_Base
//
//  Created by Techeniac Services on 09/09/21.
//

import SwiftUI

struct LoginView: View {
    @State var txtEmail : String = ""
    @State var txtPassword : String = ""
    @State var isPasswordSecure : Bool = false
    var body: some View {
        //        ScrollView{
        ZStack{
            VStack{
                Image(systemName: "star").resizable()
                    .frame(width: 100, height: 100, alignment: .center)
                Spacer().frame(height:30)
                VStack{
                    TextField("Enter a Email", text: $txtEmail)
                        .frame(width: 300.0, height: 30.0)
                    Spacer()
                    HStack                {
                        if isPasswordSecure{
                            SecureField("Enter a password", text: $txtPassword)
                            
                        }else{
                            TextField("Enter a password", text: $txtPassword)
                        }
                        
                        Spacer()
                        Button(action: {
                            isPasswordSecure = !isPasswordSecure
                            
                        }, label: {
                            Image(systemName: "eye")
                        })
                    }
                    .frame(width: 300.0, height: 30.0)
                    Spacer()
                    
                    Button(action: {
                        txtEmail = txtEmail.lowercased()
                        if txtEmail == "admin" && txtPassword == "admin"{
                            print("Login Succsessfull")
                        }else{
                            print("try again")
                        }
                    }, label: {
                        Text("Login")
                            .frame(width: 100.0, height: 30)
                            .foregroundColor(Color.white)
                            .background(/*@START_MENU_TOKEN@*//*@PLACEHOLDER=View@*/Color.black/*@END_MENU_TOKEN@*/)
                            .clipShape(Capsule())
                        
                    })
                    
                }.frame(height: 150)}
        }
    }
    //}
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}
