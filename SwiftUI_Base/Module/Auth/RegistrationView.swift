//
//  RegistrationView.swift
//  SwiftUI_Base
//
//  Created by Techeniac Services on 10/09/21.
//

import SwiftUI

struct RegistrationView: View {
    //    @State var txtName : String = ""
    //    @State var txtEmail : String = ""
    //    @State var txtPassword : String = ""
    //    @State var txtMobile : String = ""
    //    @State var strGender = 0
    //    @State var isPasswordSecure : Bool = false

    var placeholder = "Select Your Code"
    var dropDownList = ["+91", "+1", "+92", "+144", "+1", "+92", "+144", "+1", "+92", "+144", "+1", "+92", "+144", "+1", "+92", "+144", "+1", "+92", "+144", "+1", "+92", "+144", "+1", "+92", "+144"]
    
    @State private var countryCode = String()
    @State private var isCountryCodeExpand = false
    @State private var selection = 1
    @State private var txtMobileNumber = String()
    init(mobile : String) {
        _txtMobileNumber = State(initialValue: mobile)
    }
    var body: some View {
        NavigationView{
            ScrollView{
                VStack(alignment: .leading){
                    Image( "app_logo").size(width: 100, height: 35, alignment: .center)
                    Spacer().frame(height:35)
                    Group{
                        Text("Enter your mobile number")
                            .customFont(size: 28, foregroundColor: AppColor.headline, weight: .bold)
                        Text("We will send you a confirmation code")
                            .customFont(size: 12, foregroundColor: AppColor.title)
                    }
                    Spacer().frame(height:35)
                    Group{
                        // Country Code
                        Text("Country code")
                            .customFont(size: 14, foregroundColor: AppColor.title)
                        
                        Menu {
                            ForEach(dropDownList, id: \.self){ code in
                                Button(code) {
                                    self.countryCode = code
                                }
                            }
                        } label: {
                            Group{
                                HStack{
                                    //                            TextField("Select country code", text: .constant(countryCode))
                                    //                                .background(Color.white)
                                    //                                .frame( height: 50)
                                    //                                .onTapGesture {
                                    //                                    isCountryCodeExpand.toggle()
                                    //                                }
                                    Text(countryCode.isEmpty ? placeholder : countryCode)
                                        .background(Color.white)
                                        .frame( height: 50)
                                        .foregroundColor(countryCode.isEmpty ? Color.init(AppColor.subTitleGray) : Color.init(AppColor.black))
                                    
                                    
                                    Spacer()
                                    Image("ic_dropown").size(width: 20, height: 20)
                                }
                                .padding(.horizontal)
                                .background(Color.init(AppColor.white))
                                .cornerRadius(10)
                                .frame( height: 50)
                                .overlay(RoundedRectangle(cornerRadius: 10).stroke(lineWidth: 2).foregroundColor(Color.init(AppColor.borderColor).opacity(0.2)))
                                
                            }
                        }
                        //                    if isCountryCodeExpand{
                        //                        PopupView(title: "Title", message: "Messsage", buttonText1: "Ok",buttonText2: "123")
                        //                        List {
                        //                            Button("Order Now", action: {
                        //                                print("Order Now")
                        //                                countryCode = "+91"
                        //                            })
                        //                            Button("Adjust Order", action: {
                        //                                print("Adjust Now")
                        //                                countryCode = "+911"
                        //                            })
                        //                            Button("Cancel", action: {
                        //                                countryCode = "+1"
                        //
                        //                                print("Cancel Now")
                        //                            })
                        //                        }
                        //                        Menu {
                        //                            ForEach(dropDownList, id: \.self){ client in
                        //                                Button(client) {
                        //                                    self.countryCode = client
                        //                                }
                        //                            }
                        //                        } label: {
                        //                            VStack(spacing: 5){
                        //                                HStack{
                        //                                    Text(countryCode.isEmpty ? placeholder : countryCode)
                        //                                        .foregroundColor(countryCode.isEmpty ? .gray : .black)
                        //                                    Spacer()
                        //                                    Image(systemName: "chevron.down")
                        //                                        .foregroundColor(Color.orange)
                        //                                        .font(Font.system(size: 20, weight: .bold))
                        //                                }
                        //                                .padding(.horizontal)
                        //                                Rectangle()
                        //                                    .fill(Color.orange)
                        //                                    .frame(height: 2)
                        //                            }
                        //                        }
                        
                        //                    }
                        
                        // Mobile Number
                        Group{
                            Text("Enter Phone Number")
                                .customFont(size: 14, foregroundColor: AppColor.title)
                            Group{
                                TextField("e.g. 9876543210", text: $txtMobileNumber)
                                    .background(Color.white)
                                    .frame( height: 50)
                                    .keyboardType(.numberPad)
                            }
                            .padding(.horizontal)
                            .background(Color.init(AppColor.white))
                            .frame( height: 50)
                            .cornerRadius(10)
                            .overlay(RoundedRectangle(cornerRadius: 10).stroke(lineWidth: 2).foregroundColor(Color.init(AppColor.borderColor).opacity(0.2)))
                        }
                    }
                    Spacer().frame(height:35)
                    
                    Group{
                        NavigationLink(
                            destination: OtpVerificationView().navigationBarHidden(true),
                            label: {
                                HStack(alignment: .center){
                                    Spacer()
                                    //                                Button(action: {
                                    //                                    print("Get Code", countryCode, txtMobileNumber)
                                    //                                }, label: {
                                    Text("Get Code")
                                        .customFont(size: 17, foregroundColor: AppColor.white, weight: .bold)
                                        //                                })
                                        .frame(height: 50)
                                    
                                    
                                    Spacer()
                                }
                                .background(isvalid() ? Color.init(AppColor.buttonBackgroundColor) : Color.init(AppColor.lightGrayBackgroundColor))
                                .cornerRadius(10)
                            }).disabled(!isvalid())
                    }
                }
            }
            .padding(.all, 38)
            .background(Color.init(AppColor.AppBackgroundColor).ignoresSafeArea())
            .navigationBarHidden(true)
        }
    }
    func isvalid() -> Bool{
        print("Get Code", countryCode, txtMobileNumber)
        
        return (txtMobileNumber.count == 10 && !countryCode.isEmpty) ? true : false
    }
    
    struct PrimaryLabel: ViewModifier {
        func body(content: Content) -> some View {
            content
                .padding(10.0)
                //                .background(Color.red)
                .foregroundColor(Color.green)
                //                .font(.title)
                .overlay(
                    RoundedRectangle(cornerRadius: 10)
                        .stroke(Color.black, lineWidth: 1)
                )
        }
    }
}

struct RegistrationView_Previews: PreviewProvider {
    static var previews: some View {
        RegistrationView(mobile: "")
    }
}
