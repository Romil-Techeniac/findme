//
//  OtpVerificationView.swift
//  SwiftUI_Base
//
//  Created by Techeniac Services on 27/09/21.
//

import SwiftUI

struct OtpVerificationView: View {
    @State private var txtElement1 = String()
    @State private var txtElement2 = String()
    @State private var txtElement3 = String()
    @State private var txtElement4 = String()
    @State private var txtMobileNumber = "+91 9879879874"
    //    @State var currentDate = Date()
    @State var RemainingSec = 60
    @State var isLogin = false
    @State private var isNavigation = false
    
    let timer = Timer.publish(every: 1, on: .main, in: .common).autoconnect()
    
    @Environment(\.presentationMode) var presentationMode : Binding<PresentationMode>
    var body: some View {
        ScrollView{
            VStack(alignment: .leading){
                Image( "app_logo").size(width: 100, height: 35, alignment: .center)
                Spacer().frame(height:35)
                Group{
                    Text("Enter code sent to your number")
                        .customFont(size: 28, foregroundColor: AppColor.headline, weight: .bold)
                    Text("We sent it to the number \(txtMobileNumber)")
                        .customFont(size: 12, foregroundColor: AppColor.title)
                        .padding(1.0)
                    Text("Change your mobile number")
                        .customFont(size: 12, foregroundColor: AppColor.blackText)
                        .underline()
                        .onTapGesture {
                            self.presentationMode.wrappedValue.dismiss()
                        }
                        .padding(1.0)
                }
                Spacer().frame(height:35)
                Group{
                    HStack{
                        ZStack{
                            TextField("0", text: $txtElement1)
                                .keyboardType(.numberPad)
                                .multilineTextAlignment(.center)
                        }.modifier(otpView())
                        Spacer()
                        ZStack{
                            TextField("0", text: $txtElement2)
                                .keyboardType(.numberPad)
                                .multilineTextAlignment(.center)
                                .onAppear() {
                                    self.txtElement2 = self.txtElement1
                                }
                        }.modifier(otpView())
                        Spacer()
                        
                        ZStack{
                            TextField("0", text: $txtElement3)
                                .keyboardType(.numberPad)
                                .multilineTextAlignment(.center)
                                .onAppear() {
                                    self.txtElement3 = self.txtElement2
                                }
                        }.modifier(otpView())
                        
                        Spacer()
                        
                        ZStack{
                            TextField("0", text: $txtElement4)
                                .keyboardType(.numberPad)
                                .multilineTextAlignment(.center)
                                .onAppear() {
                                    self.txtElement4 = self.txtElement3
                                }
                        }.modifier(otpView())
                    }
                    .frame(maxWidth : .infinity)
                }
                Spacer().frame(height: 12)
                HStack( spacing: 2){
                    Text("Didn’t Receive code?")
                        .customFont(size: 12, foregroundColor: AppColor.title)
                    Text("Send Again")
                        .customFont(size: 12, foregroundColor: RemainingSec > 0 ? AppColor.title : AppColor.blackText)
                        .underline()
                        .onTapGesture {
                            if RemainingSec <= 0{
                                print("Resend OTP")
                            }else{
                                print("wait")
                            }
                        }
                        .padding(1.0)
                    if RemainingSec > 0{
                        Text("(\(RemainingSec)s)")
                            .customFont(size: 12, foregroundColor: AppColor.title)
                            .padding(1.0)
                            .onReceive(timer) { input in
                                if RemainingSec > 0 {
                                    RemainingSec -= 1
                                }else{
                                    timer.upstream.connect().cancel()
                                }
                                print(RemainingSec)
                            }
                    }
                }
                Spacer().frame(height: 90)
                
                Group{
                    //                    NavigationLink(destination: {
                    //                        VStack{
                    //                            if isLogin == true{
                    //
                    //                                DashboardView().navigationBarHidden(false)
                    //                            }else{
                    //                                IntroView(txtLocation: .constant("")).navigationBarHidden(true)
                    //                            }
                    //                        }
                    //                    }()) { // open ProjectsView only if the user clicked on the item "Projects" of the list etc..
                    //                        HStack(alignment: .center){
                    //                            Spacer()
                    //                            Text("Next")
                    //                                .customFont(size: 17, foregroundColor: AppColor.white, weight: .bold)
                    //                                .frame(height: 50)
                    //                            Spacer()
                    //                        }
                    //                        .background(isvalid() ? Color.init(AppColor.buttonBackgroundColor) : Color.init(AppColor.lightGrayBackgroundColor))
                    //                        .cornerRadius(10)
                    //                    }.disabled(!isvalid())
                    NavigationLink(
                        destination: VStack{
                            if isLogin == true{
                                TabBarView(viewRouter: ViewRouter()).navigationBarHidden(true)
                            }else{
                                IntroView(txtLocation: "Surati").navigationBarHidden(true)
                            }
                        },
                        isActive: $isNavigation){
                        HStack(alignment: .center){
                            Spacer()
                            Text("Next")
                                .customFont(size: 17, foregroundColor: AppColor.white, weight: .bold)
                                .frame(height: 50)
                                .onTapGesture(perform: {
                                    timer.upstream.connect().cancel()
                                    isNavigation = true
                                })
                            Spacer()
                        }
                        .background(isvalid() ? Color.init(AppColor.buttonBackgroundColor) : Color.init(AppColor.lightGrayBackgroundColor))
                        .cornerRadius(10)
                    }.disabled(!isvalid())
                }
            }
        }
        
        .padding(.all, 38)
        .background(Color.init(AppColor.AppBackgroundColor).ignoresSafeArea())
    }
    func isvalid() -> Bool{
        let code = txtElement1 + txtElement2 + txtElement3 + txtElement4
        print("code is " + code + " on " + txtMobileNumber)
        
        return !(txtElement1.isEmpty || txtElement2.isEmpty || txtElement3.isEmpty || txtElement4.isEmpty)
    }
    //    func stopTime(){
    //        isNavigation = true
    //    }
    struct otpView: ViewModifier {
        func body(content: Content) -> some View {
            content
                .frame(width: 50, height: 50)
                .background(Color.white)
                .cornerRadius(10.0)
                .overlay(RoundedRectangle(cornerRadius: 10).stroke(lineWidth: 2).foregroundColor(Color.init(AppColor.borderColor).opacity(0.2)))
            
        }
        
    }
}

struct OtpVerificationView_Previews: PreviewProvider {
    static var previews: some View {
        OtpVerificationView()
    }
}
