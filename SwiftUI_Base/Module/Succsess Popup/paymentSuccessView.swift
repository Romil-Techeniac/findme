//
//  paymentSuccessView.swift
//  SwiftUI_Base
//
//  Created by Techeniac Services on 18/10/21.
//

import SwiftUI

struct paymentSuccessView: View {
    @Binding var dismiss : Bool
    
    var body: some View {
        NavigationView{
            VStack{
                Spacer()
                Image("ic_success_payment").size(width: 112, height: 112)
                Spacer().frame(height: 16.0)
                Text("Payment Success").customFont(size: 20, foregroundColor: AppColor.black, weight: .medium)
                Spacer().frame(height: 6.0)
                Text("Your payment has been confirmed.").customFont(size: 14, foregroundColor: AppColor.subTitle, weight: .regular)
                //Floting Button
                Spacer().frame(height: 30.0)
                VStack{
                    Spacer()
                    HStack(alignment: .center){
                        Spacer()
                        NavigationLink(
                            destination: BookingStatusDetails().navigationBarHidden(true),
                            label: {
                                HStack(alignment: .bottom, spacing: 1.0) {
                                    Text("View Booking Status")
                                        .customFont(size: 17, foregroundColor: AppColor.white, weight: .bold)
                                }.padding(.horizontal, 14)
                                
                            })
                            .frame(height: 50)
                        
                        Spacer()
                    }
                    //                    .background(Color(red: 5/255, green: 30/255, blue: 49/255))
                    .background(Color.init(AppColor.buttonBackgroundColor))
                    .cornerRadius(10)
                    Spacer().frame( height: 5)
                }.frame(height: 50)
            }
            .padding(.horizontal, 20.0)
        }
    }
    
}

//struct paymentSuccessView_Previews: PreviewProvider {
//    static var previews: some View {
//        paymentSuccessView(dismiss: false)
//    }
//}
