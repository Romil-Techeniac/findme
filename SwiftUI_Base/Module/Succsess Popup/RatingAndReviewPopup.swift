//
//  RatingAndReviewPopup.swift
//  SwiftUI_Base
//
//  Created by Techeniac Services on 18/10/21.
//

import SwiftUI
import StarRating

struct RatingAndReviewPopup: View {
    //    @Binding var dismiss : Bool
    @State var dismiss : Bool
    @State var rtRatingStar = StarRatingConfiguration(numberOfStars: 5,stepType: .full, minRating: 0, borderColor: Color.init(AppColor.rtRatingStarFillColor), fillColors: [Color.init(AppColor.rtRatingStarFillColor)])
    @State var txtReview : String = ""
    var body: some View {
        NavigationView{
            VStack{
                Spacer()
                Group{
                    Image("ic_success_task").size(width: 112, height: 112)
                    //            Image("location_1").size(width: 112, height: 112)
                    Spacer().frame(height: 16.0)
                    Text("Congrats!").customFont(size: 20, foregroundColor: AppColor.black, weight: .bold)
                    Spacer().frame(height: 6.0)
                    Text("Your work is done. Please continue to explore find me.")
                        .customFont(size: 16, foregroundColor: AppColor.subTitle, weight: .regular)
                        .padding(.horizontal, 14.0)
                        .multilineTextAlignment(.center)
                    Spacer().frame(height: 36.0)
                }
                //Floting Button
                VStack(alignment: .trailing){
                    Text("How was your experience with us? Please rate our services.")
                        .customFont(size: 16, foregroundColor: AppColor.blackText, weight: .medium)
                        .multilineTextAlignment(.center)
                        .padding(.horizontal, 14.0)
                    StarRating(initialRating: 1, configuration: $rtRatingStar, onRatingChanged: { print($0) })
                        .frame(height: 80, alignment: .center)
                    HStack {
                        TextField("Leave a message, If you want", text: $txtReview)
                            .font(.system(size: 12))
                            .frame(height: 80)
                    }
                    .padding(.horizontal)
                    .background(Color.init("F3F3F3"))
                    .frame( height: 80)
                    .cornerRadius(10)
                    .overlay(RoundedRectangle(cornerRadius: 10).stroke(lineWidth: 2).foregroundColor(Color.init(AppColor.borderColor).opacity(0)).shadow(color:Color.init(AppColor.borderColor), radius: 10).opacity(0.2))
                    Spacer().frame(height: 15.0)
//                    StarRating(initialRating: 5.0, configuration: rtRatingStar>) { (newRating) in
//                        rtRatingStar.starVertices = newRating
//                    }
                }
                // Floating Button
                Group{
                    VStack{
                        Spacer()
                        HStack(alignment: .center){
                            Spacer()
                            NavigationLink(
                                destination: DashboardView().navigationBarHidden(true),
                                label: {
                                    HStack(alignment: .bottom, spacing: 1.0) {
                                        Text("Rate Now")
                                            .customFont(size: 17, foregroundColor: AppColor.white, weight: .bold)
                                    }.padding(.horizontal, 14)
                                })
                                .frame(height: 50)
                            Spacer()
                        }
                        //                    .background(Color(red: 5/255, green: 30/255, blue: 49/255))
                        .background(Color.init(AppColor.buttonBackgroundColor))
                        //
                        .cornerRadius(10)
                        Spacer().frame( height: 5)
                    }.frame(height: 50)
                    Spacer().frame(height: 20)
                    Button(action: {
                        dismiss.toggle()
                    }) {
                        Text("Maybe Later").underline()
                            .customFont(size: 17, foregroundColor: AppColor.lightBlueText, weight: .bold)
                    }
                    Spacer().frame(height: 20)

                }
            }
            .padding(.horizontal, 27.0)
        }
    }
}

struct RatingAndReviewPopup_Previews: PreviewProvider {
    static var previews: some View {
        RatingAndReviewPopup(dismiss: true)
    }
}
