//
//  BookingSlotView.swift
//  SwiftUI_Base
//
//  Created by Techeniac Services on 15/10/21.
//

import SwiftUI

struct BookingSlotView: View {
    @Environment(\.presentationMode) var presentationMode : Binding<PresentationMode>
    @State private var isNavigation : Bool = false
    
    var body: some View {
        NavigationView{
            VStack{
                ScrollView{
                    VStack(spacing: 30.0){
                        Text(":")
                    }
                    
                }
                .edgesIgnoringSafeArea(.bottom)
                //Floting Button
                VStack{
                    Spacer()
                    HStack(alignment: .center){
                        Spacer()
                        Button(action: {
                            print("Pay Now")
                            showingSuccsessSheet.toggle()
                        }, label: {
                            HStack(alignment: .bottom, spacing: 1.0) {
                                Text("Pay now")
                                    .customFont(size: 17, foregroundColor: AppColor.white, weight: .bold)
                                Spacer()
                                Text("$40.00")
                                    .customFont(size: 17, foregroundColor: AppColor.white, weight: .bold)
                                Text("(Visiting Fees)")
                                    .customFont(size: 10, foregroundColor:  "80"+AppColor.white, weight: .regular)
                                
                            }.padding(.horizontal, 14)
                            .sheet(isPresented: $showingSuccsessSheet, content: {
                                paymentSuccessView(dismiss: $showingSuccsessSheet)
                            })
                        })
                        .frame(height: 50)
                        Spacer()
                    }
                    //                    .background(Color(red: 5/255, green: 30/255, blue: 49/255))
                    .background(Color.init(AppColor.buttonBackgroundColor))
                    //
                    .cornerRadius(10)
                    .padding(.horizontal)
                    Spacer().frame( height: 5)
                }.frame(height: 50)
                
                
            }
            .background(Color.init(AppColor.AppBackgroundColor).ignoresSafeArea())
            .navigationBarTitleDisplayMode(.inline)
            .navigationBarItems(leading:
                                    Button(action: {
                                        print("Back")
                                        self.presentationMode.wrappedValue.dismiss()
                                    }
                                    ){
                                        Image("back")
                                            .size(width: 30, height: 30, alignment: .center)
                                    }
            )
            .toolbar { // <2>
                ToolbarItem(placement: .principal) {
                    Text("Checkout")
                }
            }
            
        }
    }
}
struct BookingSlotView_Previews: PreviewProvider {
    static var previews: some View {
        BookingSlotView()
    }
}
