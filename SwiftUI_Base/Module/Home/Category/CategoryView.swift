//
//  CategoryView.swift
//  SwiftUI_Base
//
//  Created by Techeniac Services on 13/09/21.
//

import SwiftUI

struct CategoryView: View {
    //        @State private var ImageName : String
    @State var Name : String

    var body: some View {
        //        NavigationLink(destination: ItemDetails(item: item)){
        ZStack{
            VStack{
                Image(Name).size(width: 40, height: 40, alignment: .center)
                Text(Name)
                    .customFont(size: 12, foregroundColor: AppColor.black, weight: .medium)
            }
        }.frame(width: 95, height: 100, alignment: .center)
        .padding([.trailing,.leading],5)
        .cornerRadius(25)
        .background(Color.white)
        .cornerRadius(10)
        .onTapGesture {
            print(Name)
        }
    }
}


struct CategoryView_Previews: PreviewProvider {
    static var previews: some View {
        CategoryView(Name: "Clinning")
    }
}
