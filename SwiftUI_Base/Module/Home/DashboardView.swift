//
//  DashboardView.swift
//  SwiftUI_Base
//
//  Created by Techeniac Services on 10/09/21.
//

import SwiftUI

struct DashboardView: View {
    @State private var search = ""
    @State private var name = "Romil Dhanani"
    @State private var location = "Surat, Gujrat. 394101"
    @State private var discountBanner = ["banner_ic","banner_ic","work_1","work_2"]
    @State private var topRatedBlockSize: [GridItem] = [GridItem(.flexible())]
    @State private var categoriColumn: [GridItem] = [GridItem(.flexible()),GridItem(.flexible()),GridItem(.flexible())]
    @State private var nearByBlockSize: [GridItem] = [GridItem(.flexible())]
    @State private var categories: [String] = ["Cleaning","Mechanic","Painting","Plumbing","Electrical","More"]
    @State private var TopRatedImage = ["work_1","work_2"]
    
    var body: some View {
        NavigationView{
            ScrollView{
                VStack{
                    Group{
                        UserDetailView(strName: $name, strLocation: $location)
                        SearchBarView(txtsearch: $search)
                    }
                    Spacer().frame(height: 20)
                    Group{
                        DiscountBannerView(images: discountBanner)
                    }
                    TopRatedBlockView(topRatedBlockSize: $topRatedBlockSize, TopRatedImage: $TopRatedImage)
                    Spacer().frame(height: 20)
                    CategoryBlockView(categorieColumn: $categoriColumn, categories: $categories)
                    Spacer().frame(height: 20)
                    NearByBlockView(nearByBlockSize: $nearByBlockSize)
                }
                .padding([.leading,.trailing], 16)
            }
            .background(Color.init(AppColor.AppBackgroundColor).ignoresSafeArea())
            .navigationBarHidden(true)
            .navigationBarTitleDisplayMode(.inline)
        }
    }
    init() {
        let appearance = UINavigationBarAppearance()
        appearance.shadowColor = .clear
        UINavigationBar.appearance().standardAppearance = appearance
        UINavigationBar.appearance().scrollEdgeAppearance = appearance
        UINavigationBar.appearance().backgroundColor = UIColor.init(AppColor.AppBackgroundColor)
    }
}

struct DashboardView_Previews: PreviewProvider {
    static var previews: some View {
        DashboardView()
    }
}

struct UserDetailView: View {
    @Binding var strName : String
    @Binding var strLocation : String
    
    var body: some View {
        HStack{
            VStack(alignment: .leading){
                Text("Hi, \(strName)")
                    .customFont(size: 18, foregroundColor: AppColor.blackText, weight: .bold)
                NavigationLink(
                    destination: LocationView().navigationBarHidden(true),
                    label: {
                        HStack(spacing: 2){
                            Image("location")
                                .size(width: 15, height: 15, alignment: .center)
                            Text("\(strLocation)")
                                .customFont(size: 14, foregroundColor: AppColor.subTitle, weight: .bold)
                            Image("ic_dropown")
                                .size(width: 15, height: 15, alignment: .center)
                            
                        }
                    })
                
            }
            Spacer()
            Image(systemName: "bell")
                .size(width: 25, height: 25, alignment: .center)
        }
        .padding(.vertical)
    }
}

struct SearchBarView: View {
    @Binding var txtsearch : String
    var body: some View {
        HStack {
            Image("search").size(width: 15, height: 15, alignment: .center)
            TextField("Search here", text: $txtsearch)
                .background(Color.white)
                .frame( height: 50)
        }
        .padding(.horizontal)
        .background(Color.init(AppColor.white))
        .frame( height: 50)
        .cornerRadius(10)
        .overlay(RoundedRectangle(cornerRadius: 10).stroke(lineWidth: 2).foregroundColor(Color.init(AppColor.borderColor).opacity(0)).shadow(color:Color.init(AppColor.borderColor), radius: 10).opacity(0.2))
    }
}

struct TopRatedBlockView: View {
    @Binding var topRatedBlockSize: [GridItem]
    
    @Binding var TopRatedImage : [String] // Array of image names to show
    
    var body: some View {
        Group{
            HStack{
                Text("Top Rated")
                    .customFont(size: 18, foregroundColor: AppColor.black, weight: .bold)
                //                        .font(Font.custom("DM Sans", size: 16))
                Spacer()
                Text("View All")
                    .underline()
                    .customFont(size: 12, foregroundColor: AppColor.subTitle)
                    .onTapGesture {
                        print("change location")
                    }
            }
            ScrollView(.horizontal) {
                LazyHGrid(rows: topRatedBlockSize, alignment: .top) {
                    ForEach(TopRatedImage, id: \.self) { TopBannerImage in
                        NavigationLink(
                            destination: BookingDetailsView(id: "\(TopBannerImage)").navigationBarHidden(true), // <1>
                            label: {
                                
                                TopBannerView(imageName: TopBannerImage
                                )
                            })
                    }
                }
            }.frame(height: 150)
        }
    }
}

struct CategoryBlockView: View {
    @Binding var categorieColumn: [GridItem]
    @Binding var categories: [String]
    
    var body: some View {
        Group{
            HStack{
                Text("Categories")
                    .customFont(size: 18, foregroundColor: AppColor.black, weight: .bold)
                Spacer()
                NavigationLink(
                    destination: CategoryListView().navigationBarHidden(true), // <1>
                    label: {
                        Text("View All")
                            .underline()
                            .customFont(size: 12, foregroundColor: AppColor.subTitle)
                    })
            }
            LazyVGrid(columns: categorieColumn, spacing: 20) {
                ForEach(CategoryData.categoryList, id: \.id) { item in
                    CategoryView(Name: item.name)
                }
            }
        }
    }
}

struct NearBy {
    static var NearByList = [ProviderModel(providerImage: "work_1", providerName: "VR Mechanic", providerCategory: "Car Repair & Service Center", providerPricePerHours: "30", providerRateing: "1.2"),
                             ProviderModel(providerImage: "work_2", providerName: "Shiva Datta", providerCategory: "Painter", providerPricePerHours: "20", providerRateing: "4.25"),
                             ProviderModel(providerImage: "work_1", providerName: "Raju PanWala", providerCategory: "Pan Center", providerPricePerHours: "25", providerRateing: "5.0")]
}

struct NearByBlockView: View {
    @Binding var nearByBlockSize: [GridItem]
//    @Binding var nearByList : NearByModel
    var body: some View {
        Group{
            HStack{
                Text("Near by you")
                    .customFont(size: 18, foregroundColor: AppColor.black, weight: .bold)
                Spacer()
                NavigationLink(
                    destination: SearchView().navigationBarHidden(true), // <1>
                    label: {
                        Text("View All")
                            .underline()
                            .customFont(size: 12, foregroundColor: AppColor.subTitle)
                    })
                //                            .onTapGesture {
                //                                print("View all near by")
                //                            }
            }
            LazyVGrid(columns: nearByBlockSize,spacing: 10) {
                ForEach(NearBy.NearByList) { nearby in
                    ProviderSmallView(nearByList: nearby)
                        //                        .padding(.horizontal, -30)
                        .onTapGesture {
                            print(nearby.providerName)
                        }
                }
            }
        }
    }
}
