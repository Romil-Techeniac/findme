//
//  TopBannerView.swift
//  SwiftUI_Base
//
//  Created by Techeniac Services on 13/09/21.
//

import SwiftUI

struct TopBannerView: View {
    //    @State private var AddFav : Bool
        @State var imageName : String

    var body: some View {
        //        NavigationLink(destination: ItemDetails(item: item)){
        HStack{
            Image(imageName).size(width: 220, height: 150, alignment: .center).opacity(1.0).cornerRadius(25)
        }.overlay(
            VStack{
                HStack(spacing: 2.0){
                    Text("Happy Housing")
                        .customFont(size: 12, foregroundColor: AppColor.topBannerBlacktext , weight: .medium)
                    Spacer()
                    HStack(spacing: 2.0){
                        Text("$\("45")")
                            .customFont(size: 12, foregroundColor: AppColor.topBannerBlacktext , weight: .bold)
                        Text("/ hr")
                            .customFont(size: 10, foregroundColor: AppColor.subTitle, weight: .regular)
                    }
//                    .font(.system(size: 12))
                    
                }
                .padding([.top],10)
                HStack{
                    Text("Happy Housing")
                        .customFont(size: 8, foregroundColor: AppColor.subTitle, weight: .regular)
                    Spacer()
                    
                }
//                .padding([.leading,.trailing],10)
                Spacer()
                HStack(spacing : 01){
                    overlayUserImages().offset(x: -20)
                    Group{
                        HStack{
                            Image("rate")
                                .size(width: 7, height: 7, alignment: .center)
                            Text("4.8 ratings")
                                .customFont(size: 10, foregroundColor: AppColor.subTitle, weight: .regular)
                                .fixedSize(horizontal: true, vertical: false)
                        }
                    }
                    Spacer()
                    Group{
                        HStack(spacing: 2){
                            Image("location")
                                .size(width: 7, height: 7, alignment: .center)
                            Text("Ahmaedabad.")
                                .customFont(size: 10, foregroundColor: AppColor.subTitle, weight: .regular)
//                                .scaledToFit()
//                                .minimumScaleFactor(0.6)
//                                .fixedSize(horizontal: false, vertical: true)
                        }
                    }
                }
                .padding([.bottom],10)
//                .offset(x: -20)
            }
            .padding(.horizontal, 10.0)
            .background(Color.white)
            .cornerRadius(15)
            .frame(width: 200, height: 80, alignment: .center)
            .offset(y:20)
        )
        
        //        }
    }
}
struct overlayUserImages: View {
    var body: some View{
        Group{
            ZStack{
                Image("work_1")
                    .size(width: 20, height: 20, alignment: .center)
                    .clipShape(Circle())
                Image("work_2")
                    .size(width: 20, height: 20, alignment: .center)
                    .clipShape(Circle())
                    .offset(x: 15)
                Image("work_1")
                    .size(width: 20, height: 20, alignment: .center)
                    .clipShape(Circle())
                    .offset(x: 30)
            }
            .frame(width: 60)
        }
    }
}
struct TopBanner_Previews: PreviewProvider {
    static var previews: some View {
        TopBannerView(imageName: "work_1")
    }
}
