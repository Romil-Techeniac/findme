//
//  DiscountBannerView.swift
//  SwiftUI_Base
//
//  Created by Techeniac Services on 21/09/21.
//

import SwiftUI

struct DiscountBannerView: View {
    
    @State var images = [String]() // Array of image names to show
    @State var activeImageIndex = 0 // Index of the currently displayed image
    let imageSwitchTimer = Timer.publish(every: 5, on: .main, in: .common)
        .autoconnect()
    
    @State var selectedPage: Int = 1
    
    
    var body: some View {
        VStack{
            Image(images[activeImageIndex])
                .resizable()
                .frame(height: 180)
                .cornerRadius(10)
                .onReceive(imageSwitchTimer) { _ in
                    self.activeImageIndex = (self.activeImageIndex + 1) % self.images.count
                    selectedPage = activeImageIndex + 1
                }
                .gesture(DragGesture(minimumDistance: 0, coordinateSpace: .local)
                            .onEnded({ value in
                                if value.translation.width < 0 {
                                    // left
                                    self.activeImageIndex = (self.activeImageIndex + 1) % self.images.count
                                }
                                print(activeImageIndex)
                                selectedPage = activeImageIndex + 1
                                
                                if value.translation.width > 0 {
                                    // right
                                    if activeImageIndex == 0{
                                        activeImageIndex = images.count - 1
                                    }else{
                                        self.activeImageIndex = (self.activeImageIndex - 1) % self.images.count
                                    }
                                    print(activeImageIndex)
                                }
                                selectedPage = activeImageIndex + 1
                                
                                if value.translation.height < 0 {
                                    // up
                                }
                                
                                if value.translation.height > 0 {
                                    // down
                                }
                            }))
//                .aspectRatio(contentMode: /*@START_MENU_TOKEN@*/.fit/*@END_MENU_TOKEN@*/)
                
            PageControl(selectedPage: $selectedPage, pages: images.count, circleDiameter: 10, circleMargin: 10)
        }
        .onTapGesture {
            print("click on", activeImageIndex)
        }
    }
}

struct DiscountBannerView_Previews: PreviewProvider {
    static var previews: some View {
        DiscountBannerView()
    }
}
