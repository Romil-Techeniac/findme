//
//  RatingAndReviewView.swift
//  SwiftUI_Base
//
//  Created by Techeniac Services on 20/10/21.
//

import SwiftUI
import StarRating
struct RatingAndReviewView: View {
    @State var isNavigation : Bool = false
    @Environment(\.presentationMode) var presentationMode : Binding<PresentationMode>
    @State var configuration = StarRatingConfiguration(borderColor: .clear, shadowColor: .clear)
    var providerRating : ProviderRatingModel = ProviderRatingData.sampleData
    var body: some View {
        NavigationView{
            VStack(spacing: 18.0) {
                HStack(){
                    Image(providerRating.providerImage)
                        .size(width: 52, height: 52).cornerRadius(5)
                    VStack(alignment: .leading, spacing: 3.0){
                        Text(providerRating.providerName).customFont(size: 12, foregroundColor: AppColor.black, weight: .bold)

                                .fixedSize(horizontal: true, vertical: false)
                        Text(providerRating.providerCategory).customFont(size: 12, foregroundColor: AppColor.subTitleGray, weight: .regular)
                            .fixedSize(horizontal: true, vertical: false)
                        HStack(alignment: .center, spacing: 1.0){
                            Text(providerRating.providerTotalRateing).customFont(size: 10, foregroundColor: AppColor.rtRatingStarFillColor, weight: .bold)
                                    .fixedSize(horizontal: true, vertical: false)
                            StarRating(initialRating: Double(providerRating.providerTotalRateing) ?? 0.0,configuration: $configuration).frame(width: 100, height: 10)
                            Text("(\(providerRating.providerTotalRateingCount))").customFont(size: 10, foregroundColor: AppColor.subTitleGray, weight: .regular)
                            .fixedSize(horizontal: true, vertical: false)
                        }
                    }
                    Spacer()
                    Divider()
                    Spacer()
                    VStack(alignment: .center){
                        Text("$" + (Double(providerRating.providerPricePerHours)?.uptoTwoDecimal() ?? "0.00") ).customFont(size: 16, foregroundColor: AppColor.blackText, weight: .bold)
                        Text("Hourly Rate").customFont(size: 10, foregroundColor: AppColor.subTitleGray, weight: .regular)
                    }
                }
                .padding(8.0)
                .frame(height: 70.0)
                .background(Color.init(AppColor.white))
                .cornerRadius(8)
                HStack{
                    VStack{
                        HStack{
                            Text(providerRating.providerTotalRateing).customFont(size: 30, foregroundColor: AppColor.blackText, weight: .bold)
                            Image("rate").size(width: 20, height: 20)
                            
                        }
                        Text("\(providerRating.providerTotalRateingCount) Ratings & Reviews").customFont(size: 12, foregroundColor: AppColor.black, weight: .regular).fixedSize(horizontal: false, vertical: true)
                            .padding(.horizontal, 10)

                    }
                    .frame(width: 120)
                    Spacer()
                    VStack(alignment: .leading, spacing: 5.0){
                        RateBarView(rateCounter: 5, ratepercentage: providerRating.provider1StarRateing)
                        RateBarView(rateCounter: 4, ratepercentage: providerRating.provider2StarRateing)
                        RateBarView(rateCounter: 3, ratepercentage: providerRating.provider3StarRateing)
                        RateBarView(rateCounter: 2, ratepercentage: providerRating.provider4StarRateing)
                        RateBarView(rateCounter: 1, ratepercentage: 90)
                    }
                    .frame(maxWidth: 320)
                    
                }
                .padding(10.0)
                .frame(height: 100.0)
                .background(Color.init(AppColor.white))
                .cornerRadius(8)

                ScrollView{
                    ForEach(1...5, id: \.self){ value in
                        SeprateRatingView()
                    }
//                    SeprateRatingView()
//                    SeprateRatingView()
//                    SeprateRatingView()
//                    SeprateRatingView()
                }
                .background(Color.init(AppColor.white))
                .cornerRadius(8)

                //Floting Button
                VStack{
                    Spacer()
                    HStack(alignment: .center){
                        Spacer()
                        NavigationLink(
                            destination: AddressListView(addressList: AddressData.addressList).navigationBarHidden(true),
                            isActive: $isNavigation,
                            label: {
                                
                                HStack(alignment: .bottom, spacing: 1.0) {
                                    Text("Write a Review")
                                        .customFont(size: 17, foregroundColor: AppColor.white, weight: .bold)
                                }.padding(.horizontal, 14)
                            })
                            .frame(height: 50)
                        Spacer()
                    }
                    .background(Color.init(AppColor.buttonBackgroundColor))
                    .cornerRadius(10)
                    .padding(.horizontal)
                    Spacer().frame( height: 5)
                }.frame(height: 50)

                
            }
            .padding(.all, 20.0)
            .navigationBarTitleDisplayMode(.inline)
            .navigationBarItems(leading:
                                    Button(action: {
                                        print("Back")
                                        self.presentationMode.wrappedValue.dismiss()
                                    }
                                    ){
                                        Image("back")
                                            .size(width: 30, height: 30, alignment: .center)
                                    }
            )
            .toolbar { // <2>
                ToolbarItem(placement: .principal) {
                    Text("Reviews & Ratings")
                        .customFont(size: 16,
                                    foregroundColor: AppColor.black,
                                    weight: .bold)
                }
            }
            .background(Color.init(AppColor.AppBackgroundColor).ignoresSafeArea())

        }
    }
}

struct RatingAndReviewView_Previews: PreviewProvider {
    static var previews: some View {
        RatingAndReviewView()
    }
}

struct RateBarView: View {
    var rateCounter : Int
    var ratepercentage : Int
    
    var body: some View {
        HStack(spacing: 6.0){
            Text("\(rateCounter)").customFont(size: 10, foregroundColor: AppColor.black, weight: .regular).fixedSize(horizontal: true, vertical: true)
//            Spacer()
            Image("rate").size(width: 8, height: 8)
            RoundedRectangle(cornerRadius: 20)
                .background(LinearGradient(gradient: Gradient(colors: [Color.init(AppColor.rtRatingBarStartColor), Color.init(AppColor.rtRatingBarEndColor)]), startPoint: .leading, endPoint: .trailing))
                .cornerRadius(2)
                .foregroundColor(.clear)
                .frame(width: CGFloat((150*ratepercentage))/100, height: 4)
            Text("\(ratepercentage)%").customFont(size: 8, foregroundColor: AppColor.subTitle, weight: .medium).fixedSize(horizontal: true, vertical: false)
            Spacer()
            
        }
    }
}

struct SeprateRatingView: View {
    @State var configuration = StarRatingConfiguration(borderColor: .clear, shadowColor: .clear)

    var body: some View {
        VStack(spacing: 9.0){
            HStack{
                Image("work_1")
                    .size(width: 30, height: 30)
                    .cornerRadius(5)
                VStack(alignment: .leading, spacing: 2.0){
                    Text("Samaira Reddy").customFont(size: 14, foregroundColor: AppColor.black, weight: .bold)
                    HStack(spacing: 1.0){
                        Text("4.0").customFont(size: 10, foregroundColor: AppColor.rtRatingStarFillColor, weight: .bold)
                            .fixedSize(horizontal: true, vertical: false)
                        
                        StarRating(initialRating: 5.3,configuration: $configuration).frame(width: 130, height: 10)
                        Spacer()
                        Text(getNotificationTime(date: Date())).customFont(size: 10, foregroundColor: AppColor.subTitle, weight: .regular)
                    }
                }
                Spacer()
            }
            Text("This chair is great addition in any room in your home, not only in the living room, featuring the mid-century design with modern trend and with that I’m fully satisfied with their quality services and material they use to make things approachable and ...")
                .customFont(size: 10, foregroundColor: AppColor.subTitle, weight: .regular)
            Divider()
                .padding(.top, 14.0)
                .padding([.leading, .trailing], 5)
        }
        .padding([.top, .leading, .trailing], 14)
        .background(Color.init(AppColor.white))
    }
}
