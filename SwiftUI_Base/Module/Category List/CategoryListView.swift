//
//  CategoryListView.swift
//  SwiftUI_Base
//
//  Created by Techeniac Services on 01/10/21.
//

import SwiftUI

struct CategoryListView: View {
    @Environment(\.presentationMode) var presentationMode : Binding<PresentationMode>
    @State private var CategoryCellSize: [GridItem] = [GridItem(.flexible())]
    
    @State var isNavigation : Bool = false
    var categoryList : [CategoryModel] = CategoryData.categoryList
    
    var body: some View {
        NavigationView{
            ScrollView{
                        LazyVGrid(columns: CategoryCellSize,spacing: 10) {
                            ForEach(categoryList, id: \.id) { category in
                                NavigationLink(
                                    destination: SearchView().navigationBarHidden(true),
                                    isActive: $isNavigation){
                                CategoryListCellView(category: category)
                            }
                        }
                    }
                Spacer()
            }.padding(.all, 20.0)
            .navigationBarTitleDisplayMode(.inline)
            .navigationBarItems(leading:
                                    Button(action: {
                                        print("Back")
                                        self.presentationMode.wrappedValue.dismiss()
                                    }
                                    ){
                                        Image("back")
                                            .size(width: 30, height: 30, alignment: .center)
                                    }
            )
            .toolbar { // <2>
                ToolbarItem(placement: .principal) {
                    Text("Categories")
                        .customFont(size: 16,
                                    foregroundColor: AppColor.black,
                                    weight: .bold)
                }
            }
                        .background(Color.init(AppColor.AppBackgroundColor).ignoresSafeArea())
        }
    }
}

struct CategoryListView_Previews: PreviewProvider {
    static var previews: some View {
        CategoryListView()
    }
}

struct CategoryListCellView: View {
//    @State var categoryImage : String
//    @State var categoryName : String
    let category : CategoryModel

    var body: some View {
        HStack(alignment: .center){
            Image(category.image).size(width: 50, height: 50)
                .cornerRadius(10)
            Spacer().frame(width: 20)
            Text(category.name)
                .customFont(size: 14,
                            foregroundColor: AppColor.blackText,
                            weight: .medium)
                .multilineTextAlignment(.leading)
            Spacer()
            
        }
        //                .frame(height: 70)
        .padding(8.0)
        .background(Color.init(AppColor.white))
        .cornerRadius(10)
    }
}
