//
//  GetStartedView.swift
//  SwiftUI_Base
//
//  Created by Techeniac Services on 21/10/21.
//

import SwiftUI
import Alamofire

struct GetStartedView: View {
    @State var isNavigation : Bool = false
    @Environment(\.presentationMode) var presentationMode : Binding<PresentationMode>
    @State var userList : usr?
    var realm : Realm?

    var body: some View {
        NavigationView{
            VStack{
                
            Image("img_get_started")
                .resizable()
                .aspectRatio(contentMode: .fit)
            Spacer()
                Text("Easy to find perfect service provider").customFont(size: 22, foregroundColor: AppColor.black, weight: .bold).multilineTextAlignment(.center).fixedSize(horizontal: false, vertical: true).padding(.horizontal, 70)
                Spacer()
                    .frame(height: 9.0)

                Text("We belive in perfect service with perfect quality assurance.").customFont(size: 14, foregroundColor: AppColor.black, weight: .medium)
                    .multilineTextAlignment(.center)
                    .fixedSize(horizontal: false, vertical: true).padding(.horizontal, 40)
                Spacer()

                //Floting Button
                VStack{
                    Spacer()
                    HStack(alignment: .center){
                        Spacer()
                        NavigationLink(
                            destination: RegistrationView(mobile: userList?.mobileNumber ?? "").navigationBarHidden(true),
                            isActive: $isNavigation,
                            label: {
                                HStack(alignment: .bottom, spacing: 1.0) {
                                    Text("Get Started")
                                        .customFont(size: 17, foregroundColor: AppColor.white, weight: .bold)
                                }.padding(.horizontal, 14)
                            })
                            .frame(height: 50)
                        Spacer()
                    }
                    .background(Color.init(AppColor.buttonBackgroundColor))
                    .cornerRadius(10)
                    .padding(.horizontal)
                    Spacer().frame( height: 5)
                }.frame(height: 50)

//        }
            }.edgesIgnoringSafeArea(.top)
        }.onAppear{
            var request = Parameters()
            request["email"] = "bhargav@techeniac.com"
            request["password"] = "Test@123"
            
            
//            TYPE: 1
//            networkClient.shared.loadData { (userlist) in
//                print(userlist)
//            }
            

//            TYPE: 2
//            networkClient.shared.loadUserList(url: "https://reqres.in/api/users", param: request) { (ResponceObjc, code) in
//                if let responseDic = ResponceObjc as? [String : Any] {
//                    if let list = responseDic["data"] as? [[String : Any]] {
//                        self.userList = Mapper<UserModel1>().mapArray(JSONArray: list)
//                        print(list)
//                    }
//                }
//            } failureResult: { (error) in
//                print(error.localizedDescription)
//            }
            
//            TYPE: 3
            networkClient.shared.request("http://3.136.198.47/api", command: "/v1/trainer/trainer_login", method: .post, parameters: request, headers: networkClient.shared.headersHTTP) { (responce, message) in
                if let responseDic = responce as? [String : Any] {
                    userList = Mapper<usr>().map(JSON: responseDic)!
                        print(userList)
                    do{
                        try! realm?.write {
                            let user = usr()
                            user.firstName = "Romil"
                            user.lastName = "Dhanani"
                            realm?.add(user , update: .all)
                        }
                    }
                    catch {
                        print("Error:\(error.localizedDescription)")
                    }
                }
            } failure: { (FailureMessage, FailureCode) in
                print(FailureMessage,FailureCode)
            }


        }
    }
}

struct GetStartedView_Previews: PreviewProvider {
    static var previews: some View {
        GetStartedView()
    }
}
