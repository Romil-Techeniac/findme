//
//  ProfileView.swift
//  SwiftUI_Base
//
//  Created by Techeniac Services on 06/10/21.
//

import SwiftUI
import SwiftUITooltip
import ImagePickerView

struct ProfileView: View {
    @Environment(\.presentationMode) var presentationMode : Binding<PresentationMode>
    var profile: ProviderModel = ProviderData.sampleData
    @State var isNotification : Bool = ProviderData.sampleData.isNotification
    @State var isToolTipshow : Bool = false
    @State var showImagePicker: Bool = false
    @State var image: UIImage?
    
    public var toolTipConfig = DefaultTooltipConfig()
    init() {
        toolTipConfig.backgroundColor = Color.init("C2" + AppColor.blackText)
        toolTipConfig.borderWidth = 0.0
    }
    var body: some View {
        NavigationView{
            VStack {
                ScrollView{
                    VStack(spacing: 20){
                        ProfileImageBlockView(profile: profile,showImagePicker: $showImagePicker, image: $image)
                        ProfileDetailBlockView(profile: profile)
                        IsNotificatiosEnableView(profile: profile, isNotification: $isNotification, isToolTipshow: $isToolTipshow)
                        if isToolTipshow{
                            //                        if true{
                            VStack{
                                Text("1").foregroundColor(.clear)
                            }.frame(height: 1)
                            .tooltip(.top, config: toolTipConfig){
                                ToolTipsView()
                            }
                            .offset(x: -10, y: -50)
                        }
                        
                    }
                    .padding(.horizontal, 20.0)
                }
                //Floting Button
                VStack{
                    Spacer()
                    //                    NavigationLink(
                    //                        destination:VStack{ Text("Go To Gome")},
                    //                        isActive: $isNavigation){
                    HStack(alignment: .center){
                        Spacer()
                        Text("Confirm Details")
                            .customFont(size: 17, foregroundColor: AppColor.white, weight: .bold)
                            .frame(height: 50)
                        Spacer()
                    }
                    .onTapGesture {
                        print("Confirm")
                        self.presentationMode.wrappedValue.dismiss()
                    }
                    //                    }
                    .background(Color.init(AppColor.buttonBackgroundColor))
                    .cornerRadius(10)
                    .padding(.horizontal)
                    Spacer().frame( height: 5)
                }.frame( height: 50)
            }
            .navigationBarTitleDisplayMode(.inline)
//            .navigationBarItems(leading:
//                                    Button(action: {
//                                        print("Back")
//                                        self.presentationMode.wrappedValue.dismiss()
//                                    }
//                                    ){
//                                        Image("back")
//                                            .size(width: 30, height: 30, alignment: .center)
//                                    }
//            )
            .toolbar { // <2>
                ToolbarItem(placement: .principal) {
                    Text("Profile Details")
                        .customFont(size: 16,
                                    foregroundColor: AppColor.black,
                                    weight: .bold)
                }
            }
            .background(Color.init(AppColor.AppBackgroundColor).ignoresSafeArea())
        }
    }
}

struct ProfileView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileView()
    }
}

struct ProfileImageBlockView: View {
    var profile: ProviderModel
    @Binding var showImagePicker: Bool
    @Binding var image: UIImage?
    
    var body: some View {
        VStack(spacing: 1.0){
            ZStack{
                Image("ic_profile_enimation_circle" ).size(width: 150, height: 150)
                if image != nil {
                    Image(uiImage: image!)
                        .size(width: 95, height: 95)
                        .clipped()
                        .clipShape(Circle())
                        .overlay(Circle().strokeBorder(Color.init(AppColor.white),lineWidth: 1))
                }else{
                    Image("\(profile.providerImage)").size(width: 95, height: 95)
                        .clipShape(Circle())
                        .overlay(Circle().strokeBorder(Color.init(AppColor.white),lineWidth: 1))
                }
                VStack{
                    Image("ic_edit_profile").size(width: 24, height: 24).onTapGesture {
                        print("open photo picker")
                        self.showImagePicker.toggle()
                        
                    }
                }
                .sheet(isPresented: $showImagePicker) {
                    ImagePickerView(sourceType: .photoLibrary) { image in
                        self.image = image
                    }
                }
                .padding(5.0)
                .background(Color.init(AppColor.white))
                .cornerRadius(7)
                .offset(x: 35, y: -35)
                
            }
            
            Text("\(profile.providerName)").customFont(size: 14, foregroundColor: AppColor.blackText, weight: .bold)
            if !profile.providerContactNumber.isEmpty{
                Text("\(profile.providerContactNumber)").customFont(size: 10, foregroundColor: AppColor.subTitle, weight: .regular)
            }
        }
    }
}

struct ProfileDetailBlockView: View {
    var profile: ProviderModel
    var body: some View {
        VStack{
            VStack(alignment: .leading, spacing: 8.0){
                Text("Name").customFont(size: 13, foregroundColor: AppColor.subTitle, weight: .regular)
                TextField("Enter Name", text: .constant(profile.providerName))
                Divider()
            }
            .padding(.top)
            VStack(alignment: .leading, spacing: 8.0){
                Text("Email").customFont(size: 13, foregroundColor: AppColor.subTitle, weight: .regular)
                TextField("Enter Email", text: .constant(profile.providerEmail))
                Divider()
            }
            .padding(.top)
            VStack(alignment: .leading, spacing: 8.0){
                Text("Contact Number").customFont(size: 13, foregroundColor: AppColor.subTitle, weight: .regular)
                TextField("Enter Contact Number", text: .constant(profile.providerContactNumber)).keyboardType(.numberPad)
                Divider()
            }
            .padding(.vertical)
        }
        .padding(.horizontal)
        .background(Color.init(AppColor.white))
        .cornerRadius(10)
    }
}

struct ToolTipsView: View {
    var body: some View {
        ZStack {
            VStack(alignment: .leading){
                Text("Note:").customFont(size: 10, foregroundColor: AppColor.white, weight: .bold)
                Text("This notification alert is about to... ").customFont(size: 10, foregroundColor: AppColor.white, weight: .bold)
            }
            .padding(8)
            .cornerRadius(5)
            .foregroundColor(Color.init(AppColor.white))
        }
    }
}

struct IsNotificatiosEnableView: View {
    var profile: ProviderModel
    @Binding var isNotification : Bool
    @Binding var isToolTipshow : Bool
    
    var body: some View {
        HStack{
            Text("Notifications Alert").customFont(size: 16, foregroundColor: AppColor.black, weight: .medium)
            Image("ic_info").size(width: 12, height: 12).onTapGesture {
                print("Info")
                isToolTipshow.toggle()
            }
            Spacer()
            
            Toggle("", isOn: $isNotification)
                .toggleStyle(SwitchToggleStyle(tint: Color.init("#1B689C")))
                .onChange(of: profile.isNotification) { value in
                    print(value)
                }
                .onReceive([self.isNotification].publisher.first()) { (value) in
                    print("New value is: \(value)")
                }
            //                            .toggleStyle(SwitchToggleStyle(tint: Gradient(colors: [Color.init("0BB4FF"),Color.init("#1B689C")])))
            
            
        }
        .frame(height: 60.0)
        .padding(.horizontal)
        .background(Color.init(AppColor.white))
        .cornerRadius(10)
    }
}
