//
//  NetworkClient.swift
//  SwiftUI_Base
//
//  Created by Techeniac Services on 26/10/21.
//

import Foundation
import Alamofire

typealias SuccessHandler = (_ result:Any , _ status : Int) -> Void
typealias FailureHandler = (_ error:Error) -> Void
public typealias FailureMessage = String
public typealias ResponseString = String
public typealias FailureCode    = String
public typealias ResponseData = [String: Any]
public typealias ResponseArray = [Any]

class networkClient{
    
    // MARK: - Constant
    struct Constants {
        
        //MARK:  ResponseKey
        struct ResponseKey {
            static let code                         = "code"
            static let data                         = "data"
            static let message                      = "message"
            static let list                         = "list"
            static let serverLastSync               = "server_last_sync"
        }

    //MARK:  ResponseCode
    struct ResponseCode {
        
        static let EmailRequired                     = "EMAIL_IS_REQUIRED"
        static let DobRequired                       = "DOB_IS_REQUIRED"
        static let EmailDobRequired                  = "EMAIL_DOB_IS_REQUIRED"
        static let ok                                = "OK"
        static let kTokenExpiredCode                 = "E_TOKEN_EXPIRED"
        static let kUnAuthorized                     = "E_UNAUTHORIZED"
        static let kListNotFound                     = "E_NOT_FOUND"
        static let kNoInternet                       = "NO_INTERNET"
        static let kBadAccess                        = "E_BAD_REQUEST"
    }
        
    }
    // A Singleton instance
    static let shared: networkClient = networkClient()
    let networkReachability = NetworkReachabilityManager()

    var headersHTTP: HTTPHeaders {
        return [// "accept": "application/json",
//            "os-name": "\(UIDevice.current.name)",
            "os-name": "\(UIDevice.modelName)",
            "os-version": "\(UIDevice.current.systemVersion)",
            "platform": "iOS",
            "AppVersion": "\(Bundle.main.infoDictionary?["CFBundleShortVersionString"] ?? "0.0")",
            "AppBuildVersion": "\(Bundle.main.infoDictionary?["CFBundleVersion"] ?? "0.0")"
        ]
    }
    
    // Initialize
    private init() {
        
        networkReachability?.startListening(onUpdatePerforming: { (status) in
            print(status)
        })
        
//        setIndicatorViewDefaults()
        
    }
   
    // Global function to call web service
    func request(_ url: URLConvertible, command: String, method: Alamofire.HTTPMethod = .get, parameters: Parameters? = nil, headers: HTTPHeaders? = nil, success:@escaping ((Any, String)->Void), failure:@escaping ((FailureMessage,FailureCode)->Void)) {
        
        // check network reachability
        guard (networkReachability?.isReachable)! else {
            
            failure("No Internet Connection.", Constants.ResponseCode.kNoInternet)
            return
        }
        
        // create final url
        let finalURLString: String = "\(url)\(command)"
        let finalURL = NSURL(string : finalURLString)! as URL
        
        print(finalURL)
        
        // Network request
        AF.request(finalURL, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response: AFDataResponse<Any>) in
            
//            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            switch response.result {
            case .success(let value):
                
                let responseObject = value as? [String: Any]
                
                // get status code
                let statusCode = "\(responseObject?[Constants.ResponseKey.code] ?? 0)"
                
                // get status message
                let message = responseObject?[Constants.ResponseKey.message] as? String ?? ""
                
                //User LogOut
                if statusCode == Constants.ResponseCode.kTokenExpiredCode {
                    failure(message,statusCode)
                    return
                }
                
                //User LogOut
                if statusCode == Constants.ResponseCode.kUnAuthorized {
//                    ApplicationData.sharedInstance.clearUserData()
//                    ApplicationData.sharedInstance.moveToLogin()
                    failure(message,statusCode)
                    return
                }
                
//                //Chek Code
//                if statusCode != Constants.ResponseCode.ok {
//                    failure(message,statusCode)
//                    return
//                }
                
                // get data object
                let dataObject = responseObject?[Constants.ResponseKey.data]
                
                // pass data as dictionary if data key contains json object
                if let data = dataObject as? ResponseData {
                    success(data, message)
                    return
                }
                else if let data = dataObject as? ResponseArray {
                    success(data, message)
                    return
                }else{
                    failure(message,statusCode)
                    return
                }
                
                
            case .failure(let error):
                let isServerTrustEvaluationError =
                    error.asAFError?.isServerTrustEvaluationError ?? false
                var message: String
                if isServerTrustEvaluationError {
                    message = "Certificate Pinning Error"
                } else {
                    message = error.localizedDescription
                }
                message = error.localizedDescription
                // check result is success
                failure(message,"")
                return
            }
        }
        
    }
    
}

public extension UIDevice {
    static let modelName: String = {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }

        func mapToDevice(identifier: String) -> String { // swiftlint:disable:this cyclomatic_complexity
            #if os(iOS)
            switch identifier {
            case "iPod5,1":                                       return "iPod touch (5th generation)"
            case "iPod7,1":                                       return "iPod touch (6th generation)"
            case "iPod9,1":                                       return "iPod touch (7th generation)"
            case "iPhone3,1", "iPhone3,2", "iPhone3,3":           return "iPhone 4"
            case "iPhone4,1":                                     return "iPhone 4s"
            case "iPhone5,1", "iPhone5,2":                        return "iPhone 5"
            case "iPhone5,3", "iPhone5,4":                        return "iPhone 5c"
            case "iPhone6,1", "iPhone6,2":                        return "iPhone 5s"
            case "iPhone7,2":                                     return "iPhone 6"
            case "iPhone7,1":                                     return "iPhone 6 Plus"
            case "iPhone8,1":                                     return "iPhone 6s"
            case "iPhone8,2":                                     return "iPhone 6s Plus"
            case "iPhone8,4":                                     return "iPhone SE"
            case "iPhone9,1", "iPhone9,3":                        return "iPhone 7"
            case "iPhone9,2", "iPhone9,4":                        return "iPhone 7 Plus"
            case "iPhone10,1", "iPhone10,4":                      return "iPhone 8"
            case "iPhone10,2", "iPhone10,5":                      return "iPhone 8 Plus"
            case "iPhone10,3", "iPhone10,6":                      return "iPhone X"
            case "iPhone11,2":                                    return "iPhone XS"
            case "iPhone11,4", "iPhone11,6":                      return "iPhone XS Max"
            case "iPhone11,8":                                    return "iPhone XR"
            case "iPhone12,1":                                    return "iPhone 11"
            case "iPhone12,3":                                    return "iPhone 11 Pro"
            case "iPhone12,5":                                    return "iPhone 11 Pro Max"
            case "iPhone12,8":                                    return "iPhone SE (2nd generation)"
            case "iPhone13,1":                                    return "iPhone 12 mini"
            case "iPhone13,2":                                    return "iPhone 12"
            case "iPhone13,3":                                    return "iPhone 12 Pro"
            case "iPhone13,4":                                    return "iPhone 12 Pro Max"
            case "iPhone14,4":                                    return "iPhone 13 mini"
            case "iPhone14,5":                                    return "iPhone 13"
            case "iPhone14,2":                                    return "iPhone 13 Pro"
            case "iPhone14,3":                                    return "iPhone 13 Pro Max"
            case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":      return "iPad 2"
            case "iPad3,1", "iPad3,2", "iPad3,3":                 return "iPad (3rd generation)"
            case "iPad3,4", "iPad3,5", "iPad3,6":                 return "iPad (4th generation)"
            case "iPad6,11", "iPad6,12":                          return "iPad (5th generation)"
            case "iPad7,5", "iPad7,6":                            return "iPad (6th generation)"
            case "iPad7,11", "iPad7,12":                          return "iPad (7th generation)"
            case "iPad11,6", "iPad11,7":                          return "iPad (8th generation)"
            case "iPad12,1", "iPad12,2":                          return "iPad (9th generation)"
            case "iPad4,1", "iPad4,2", "iPad4,3":                 return "iPad Air"
            case "iPad5,3", "iPad5,4":                            return "iPad Air 2"
            case "iPad11,3", "iPad11,4":                          return "iPad Air (3rd generation)"
            case "iPad13,1", "iPad13,2":                          return "iPad Air (4th generation)"
            case "iPad2,5", "iPad2,6", "iPad2,7":                 return "iPad mini"
            case "iPad4,4", "iPad4,5", "iPad4,6":                 return "iPad mini 2"
            case "iPad4,7", "iPad4,8", "iPad4,9":                 return "iPad mini 3"
            case "iPad5,1", "iPad5,2":                            return "iPad mini 4"
            case "iPad11,1", "iPad11,2":                          return "iPad mini (5th generation)"
            case "iPad14,1", "iPad14,2":                          return "iPad mini (6th generation)"
            case "iPad6,3", "iPad6,4":                            return "iPad Pro (9.7-inch)"
            case "iPad7,3", "iPad7,4":                            return "iPad Pro (10.5-inch)"
            case "iPad8,1", "iPad8,2", "iPad8,3", "iPad8,4":      return "iPad Pro (11-inch) (1st generation)"
            case "iPad8,9", "iPad8,10":                           return "iPad Pro (11-inch) (2nd generation)"
            case "iPad13,4", "iPad13,5", "iPad13,6", "iPad13,7":  return "iPad Pro (11-inch) (3rd generation)"
            case "iPad6,7", "iPad6,8":                            return "iPad Pro (12.9-inch) (1st generation)"
            case "iPad7,1", "iPad7,2":                            return "iPad Pro (12.9-inch) (2nd generation)"
            case "iPad8,5", "iPad8,6", "iPad8,7", "iPad8,8":      return "iPad Pro (12.9-inch) (3rd generation)"
            case "iPad8,11", "iPad8,12":                          return "iPad Pro (12.9-inch) (4th generation)"
            case "iPad13,8", "iPad13,9", "iPad13,10", "iPad13,11":return "iPad Pro (12.9-inch) (5th generation)"
            case "AppleTV5,3":                                    return "Apple TV"
            case "AppleTV6,2":                                    return "Apple TV 4K"
            case "AudioAccessory1,1":                             return "HomePod"
            case "AudioAccessory5,1":                             return "HomePod mini"
            case "i386", "x86_64":                                return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "iOS"))"
            default:                                              return identifier
            }
            #elseif os(tvOS)
            switch identifier {
            case "AppleTV5,3": return "Apple TV 4"
            case "AppleTV6,2": return "Apple TV 4K"
            case "i386", "x86_64": return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "tvOS"))"
            default: return identifier
            }
            #endif
        }

        return mapToDevice(identifier: identifier)
    }()
}
