//
//  SyncManager.swift
//  SwiftUI_Base
//
//  Created by Techeniac Services on 17/11/21.
//

import Foundation

class SyncManager: NSObject {

    // A Singleton instance
    static let sharedInstance = SyncManager()
    let realm = try! Realm()
    
    // Initialize
    private override init() {
        print("REALM PATH : \(Realm.Configuration.defaultConfiguration.fileURL!)")
    }
}
