//
//  TabBarView.swift
//  SwiftUI_Base
//
//  Created by Techeniac Services on 28/09/21.
//

import SwiftUI

struct TabBarView: View {
    @StateObject var viewRouter: ViewRouter
    
    var body: some View {
        GeometryReader{ geometry in
            VStack{
                Spacer()
                switch viewRouter.CurrentPage{
                case .home:
                    DashboardView()
               
                case .search:
                    FilterView()
                    
                case .booking:
//                    AddressListView( addressList: AddressData.addressList)
                    BookingHistoryView()
//                    RatingAndReviewView()
                case .profile:
                    ProfileView()
                }
                Spacer()
                ZStack{
                    HStack{
                        Spacer()
                        TabBarIcon(viewRouter: viewRouter, assignedPage: .home, width: geometry.size.width/5, height: geometry.size.height/28, systemIconName: "ic_home", tabName: "Home")
                        Spacer()
                        

                        TabBarIcon(viewRouter: viewRouter, assignedPage: .search, width: geometry.size.width/5, height: geometry.size.height/28, systemIconName: "ic_search", tabName: "Search")
                        Spacer()

                        TabBarIcon(viewRouter: viewRouter, assignedPage: .booking, width: geometry.size.width/5, height: geometry.size.height/28, systemIconName: "ic_booking", tabName: "Booking")
                                                Spacer()
                        TabBarIcon(viewRouter: viewRouter, assignedPage: .profile, width: geometry.size.width/5, height: geometry.size.height/28, systemIconName: "ic_profile", tabName: "Profile")
                        Spacer()

                    }
                }.frame(height: 80, alignment: .center)
            }
            .edgesIgnoringSafeArea(.vertical)
        }
    }
}

    struct TabBarView_Previews: PreviewProvider {
        static var previews: some View {
            TabBarView(viewRouter: ViewRouter())
        }
    }

struct TabBarIcon: View {
    
    @StateObject var viewRouter: ViewRouter
    let assignedPage: Page
    
    let width, height: CGFloat
    let systemIconName, tabName: String
    
    var body: some View {
        VStack {
            Image(systemIconName)
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: width, height: height)
                .padding(.top, 10)
            Text(tabName)
//÷                .customFont(size: 12, weight: .medium)
                .font(.system(size: 12))
            Spacer()
        }
        .padding(.horizontal, -4)
        .onTapGesture {
            viewRouter.CurrentPage = assignedPage
//            viewRouter.currentPage = assignedPage
            print(assignedPage)
        }
        .foregroundColor(viewRouter.CurrentPage == assignedPage ?   Color.init(AppColor.blackText) : Color.init(AppColor.subTitleGray))
    }
}
