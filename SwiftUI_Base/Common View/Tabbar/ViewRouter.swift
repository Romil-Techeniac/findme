//
//  ViewRouter.swift
//  SwiftUI_Base
//
//  Created by Techeniac Services on 28/09/21.
//

import Foundation
import SwiftUI
class ViewRouter: ObservableObject {
    @Published var CurrentPage : Page = .home
}
enum Page {
    case home
    case booking
    case search
    case profile
}
