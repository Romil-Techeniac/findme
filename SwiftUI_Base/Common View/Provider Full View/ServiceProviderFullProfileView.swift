//
//  ServiceProviderFullProfileView.swift
//  SwiftUI_Base
//
//  Created by Techeniac Services on 16/09/21.
//

import SwiftUI

struct RoundedCorner: Shape {
    
    var radius: CGFloat = .infinity
    var corners: UIRectCorner = .allCorners
    func path(in rect: CGRect) -> Path {
        let path = UIBezierPath(roundedRect: rect, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        return Path(path.cgPath)
    }
}
struct ServiceProviderFullProfileView: View {
//    @Binding var isTopRated : Bool
//    @Binding var providerImage : String
//    @Binding var providerName : String
//    @Binding var providerCategory : String
//    @Binding var providerRate : String
//    @Binding var providerExperiance : String
//    @Binding var providerLocation : String
//    @Binding var providerDetails : ProviderModel
    var providerDetails : ProviderModel

    var body: some View {
        Group{
            HStack(alignment: .top){
                Image(providerDetails.providerImage).size(width: 100   , height: 100, alignment: .center).cornerRadius(5)
                //                Spacer()
                VStack(alignment: .leading){
                    HStack{
                        Text(providerDetails.providerName)
                            .customFont(size: 14, foregroundColor: AppColor.black, weight: .bold)
                    }
                    HStack{
                        Text("(\(providerDetails.providerCategory))")
                            .customFont(size: 10, foregroundColor: AppColor.subTitleGray)
                    }
                    HStack(spacing: 3.0){
                        Image("experiance").size(width: 14, height: 14, alignment: .center)
                        Text("\(providerDetails.providerExperiance) \(providerDetails.providerExperiance == "1" ? "Year" : "Years") of experiance")
                            .customFont(size: 10, foregroundColor: AppColor.subTitleGray)
                    }
                    HStack(spacing: 3.0){
                        Image("location_gray").size(width: 14, height: 14, alignment: .center)
                        Text(providerDetails.providerLocation)
                            .customFont(size: 10, foregroundColor: AppColor.subTitleGray)
                    }
                    HStack(spacing: 7.0){
                        HStack(spacing: 4.0){
                            Image("rate").size(width: 10, height: 10, alignment: .center)
                            Image("rate").size(width: 10, height: 10, alignment: .center)
                            Image("rate").size(width: 10, height: 10, alignment: .center)
                            Image("rate").size(width: 10, height: 10, alignment: .center)
                            Image("rate").size(width: 10, height: 10, alignment: .center)
                        }
                        Text("\(providerDetails.providerRateing) /")
                            .customFont(size: 10, foregroundColor: AppColor.black, weight: .bold)
                        Text("5")
                            .customFont(size: 10, foregroundColor: AppColor.subTitleGray)
                            .offset(x: -5)
                    }
                }
                Spacer()
                if providerDetails.isTopRated{
                    HStack(alignment: .top){
                        Image("top_rated").size(width: 15, height: 15)
                    }.frame(width: 25, height: 30).background(Color.init(AppColor.lightBlueBackgroundColor))
                    .cornerRadius(7, corners: [.bottomLeft, .bottomRight])
                    .offset(y: -16)
                }
                
            }
            .padding(16.0)
            
        }
        
    }
    struct TopRated: View {
        var body: some View {
            ZStack{
                VStack{
                    Image("top_rated").accentColor(.yellow).foregroundColor(.red)
                    
                }.frame(width: 35, height: 30).background(Color.red)
            }
        }
    }
    
}

struct ServiceProviderFullProfileView_Previews: PreviewProvider {
    static var previews: some View {
        ServiceProviderFullProfileView(providerDetails: ProviderData.sampleData)
    }
}
