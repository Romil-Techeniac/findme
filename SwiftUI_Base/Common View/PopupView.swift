//
//  PopupView.swift
//  SwiftUI_Base
//
//  Created by Techeniac Services on 27/09/21.
//

import SwiftUI

struct PopupView: View {
    var title: String
    var message: String
    var buttonText1: String
    var buttonText2: String
//    @Binding var show: Bool

    var body: some View {
        ZStack {
//            if show {
                // PopUp background color
                Color.black.opacity(true ? 0.3 : 0).edgesIgnoringSafeArea(.all)

                // PopUp Window
                VStack(alignment: .center, spacing: 0) {
                    Text(title)
                        .frame(maxWidth: .infinity)
                        .frame(height: 45, alignment: .center)
                        .font(Font.system(size: 23, weight: .semibold))
                        .foregroundColor(Color.white)
                        .background(Color(#colorLiteral(red: 0.6196078431, green: 0.1098039216, blue: 0.2509803922, alpha: 1)))

                    Text(message)
                        .multilineTextAlignment(.center)
                        .font(Font.system(size: 16, weight: .semibold))
                        .padding(EdgeInsets(top: 20, leading: 25, bottom: 20, trailing: 25))
                        .foregroundColor(Color.white)

                    HStack{
                        Button(action: {
                            // Dismiss the PopUp
                            withAnimation(.linear(duration: 0.3)) {
//                                show = false
                                print(buttonText1)
                            }
                        }, label: {
                            Text(buttonText1)
                                .frame(maxWidth: .infinity)
                                .frame(height: 54, alignment: .center)
                                .foregroundColor(Color.white)
                                .background(Color(#colorLiteral(red: 0.6196078431, green: 0.1098039216, blue: 0.2509803922, alpha: 1)))
                                .font(Font.system(size: 23, weight: .semibold))
                        }).buttonStyle(PlainButtonStyle())
                        Button(action: {
                            // Dismiss the PopUp
                            withAnimation(.linear(duration: 0.3)) {
                                print(buttonText2)
                            }
                        }, label: {
                            Text(buttonText2)
                                .frame(maxWidth: .infinity)
                                .frame(height: 54, alignment: .center)
                                .foregroundColor(Color.white)
                                .background(Color(#colorLiteral(red: 0.6196078431, green: 0.1098039216, blue: 0.2509803922, alpha: 1)))
                                .font(Font.system(size: 23, weight: .semibold))
                        }).buttonStyle(PlainButtonStyle())

                    }.padding([.leading, .bottom, .trailing])
//                    Button(action: {
//                        // Dismiss the PopUp
//                        withAnimation(.linear(duration: 0.3)) {
//                            show = false
//                        }
//                    }, label: {
//                        Text(buttonText)
//                            .frame(maxWidth: .infinity)
//                            .frame(height: 54, alignment: .center)
//                            .foregroundColor(Color.white)
//                            .background(Color(#colorLiteral(red: 0.6196078431, green: 0.1098039216, blue: 0.2509803922, alpha: 1)))
//                            .font(Font.system(size: 23, weight: .semibold))
//                    }).buttonStyle(PlainButtonStyle())
                }
                .frame(maxWidth: 300)
                .border(Color.white, width: 2)
                .background(Color(#colorLiteral(red: 0.737254902, green: 0.1294117647, blue: 0.2941176471, alpha: 1)))
//            }
        }
    }
}

struct PopupView_Previews: PreviewProvider {

    static var previews: some View {
        PopupView(title: "Title", message: "Message", buttonText1: "ok", buttonText2: "123")
    }
}
