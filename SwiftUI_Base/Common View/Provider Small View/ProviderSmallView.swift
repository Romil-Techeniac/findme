//
//  ProviderSmallView.swift
//  SwiftUI_Base
//
//  Created by Techeniac Services on 13/09/21.
//

import SwiftUI

struct ProviderSmallView: View {
    let nearByList : ProviderModel
    var body: some View {
        //        NavigationLink(destination: ItemDetails(item: item)){
        //        ZStack{
        HStack{
            Image(nearByList.providerImage).size(width: 50, height: 50, alignment: .center).cornerRadius(5)
            Spacer().frame(width: 10)
            VStack(alignment: .leading, spacing: 5.0){
                Text(nearByList.providerName)
                    .customFont(size: 12, foregroundColor: AppColor.black, weight: .bold)
                Text(nearByList.providerCategory).foregroundColor(Color.init(AppColor.subTitleGray))
                    .customFont(size: 12, foregroundColor: AppColor.subTitleGray)
                Text("$\(nearByList.providerPricePerHours)/hr.")
                    .customFont(size: 12, foregroundColor: AppColor.blackText, weight: .bold)
            }
            Spacer()
            
            VStack{
                HStack{
                    Text(verbatim: nearByList.providerRateing).customFont(size: 10, foregroundColor: AppColor.black, weight: .bold)
                    Image("rate")
                        .size(width: 10, height: 10, alignment: .center)
                }
                .padding(.horizontal, 10.0)
                .padding(.vertical, 5.0)
                //                    .overlay(clipShape(Capsule()))
                .background(Color.init(AppColor.ratingBackgroundColor))
                .cornerRadius(10)
                
                Spacer()
                
            }
            .padding([.trailing,.top],10)
        }.frame(height: 70, alignment: .center)
        //        }.frame(width: 100, height: 100, alignment: .center)
        .padding([.trailing,.leading,.bottom], 5)
                .cornerRadius(25)
//                .background(Color.yellow)
        //        .cornerRadius(10)
        .background(Color.white)
    }
}

struct NearByView_Previews: PreviewProvider {
    static var previews: some View {
        ProviderSmallView(nearByList: NearBy.NearByList.first!)
    }
}
