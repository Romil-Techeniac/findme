//
//  ColorConstant.swift
//  SwiftUI_Base
//
//  Created by Techeniac Services on 22/09/21.
//

import Foundation
struct AppColor {
    static let AppBackgroundColor = "F5FBFF" //F5FBFF
    
    static let headline = "1E263C"
    static let white = "FFFFFF"
    static let black = "000000"
    static let title = "565656"
    static let subTitle = "979797"
    static let subTitleGray = "B0B0B0"
    static let subTitleDark = "7C7C7C"
    static let borderColor = "C4C4C4"
    static let buttonBackgroundColor = "051E32"
    
    static let lightBlueBackgroundColor = "E2F3FF"
    static let lightBlueText = "0BB4FF"
    static let lightRedText = "F5948D"
    static let lightGrayBackgroundColor = "E6E6E6"
    static let trackstatusText = "3E4954"
    static let blackText = "02253D"
    static let ratingBackgroundColor = "FFF4D1"
    static let topBannerBlacktext = "333333"
    static let textfeildBackground = "F4F4F4"
    static let rtRatingStarFillColor = "F4C534"
    static let rtRatingStarEmptyColor = "DADADA"
    static let rtRatingBarStartColor = "02253D"
    static let rtRatingBarEndColor = "0BB4FF"
    struct registrationView {
    }
}
