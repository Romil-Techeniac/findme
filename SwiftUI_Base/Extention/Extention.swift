//
//  Extention.swift
//  SwiftUI_Base
//
//  Created by Techeniac Services on 21/09/21.
//

import Foundation
import SwiftUI


extension String{
    func replaceSpaceTo20 () -> String {

            var str = self.trimmingCharacters(in: .whitespaces)

            str = str.replacingOccurrences(of: " ", with: "%20")

            return str

        }

}
