//
//  ImageExtention.swift
//  SwiftUI_Base
//
//  Created by Techeniac Services on 05/10/21.
//

import Foundation
import SwiftUI
extension Image{
    func size(width : CGFloat, height : CGFloat, alignment : Alignment = .center) -> some View{
        self
            .resizable()
            
            .scaledToFill()
            
            .aspectRatio(contentMode: .fill)
            .frame(width: width, height: height, alignment: alignment)
            .clipped()
    }
}
