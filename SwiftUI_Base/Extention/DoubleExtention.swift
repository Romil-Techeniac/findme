//
//  DoubleExtention.swift
//  SwiftUI_Base
//
//  Created by Techeniac Services on 05/10/21.
//

import Foundation
extension Double{
    func uptoTwoDecimal() -> String {
        return String(format: "%.2f", self)
    }
}
