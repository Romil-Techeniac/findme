//
//  TextExtention.swift
//  SwiftUI_Base
//
//  Created by Techeniac Services on 05/10/21.
//

import Foundation
import SwiftUI

extension Text{
    
    func customFont(size : CGFloat = 10, foregroundColor : String = AppColor.black, weight : Font.Weight = .regular, fontFamily : String = "DM Sans") -> Text {
        self
            .foregroundColor(Color.init(foregroundColor))
            .font(.system(size: size))
            .fontWeight(weight)
    }
}
