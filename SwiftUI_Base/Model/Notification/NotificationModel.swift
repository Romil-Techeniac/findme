//
//  NotificationModel.swift
//  SwiftUI_Base
//
//  Created by Techeniac Services on 19/10/21.
//

import Foundation
struct NotificationModel: Hashable, Identifiable {
    let id = UUID()
    var isReaded : Bool = false
    var title : String
    var category : String
    var date = Date()
}

struct NotificationData {
    static let sampleData = NotificationModel(isReaded: false, title: "The solutions offer 24/7 services without any break.", category: "Plumber", date: Date())
    
    static var NotificationList =
        [
            NotificationModel(isReaded: false, title: "The solutions offer 24/7 services without any break.", category: "Plumbing", date: Date()),
            NotificationModel(isReaded: true, title: "The solutions offer 24/7 services without any break.", category: "More", date: Date().addingTimeInterval(780000)),
            NotificationModel(isReaded: true, title: "Let’s have a discussion on the working of this system.", category: "Painting", date: Date().addingTimeInterval(15000) )
        ]
}

