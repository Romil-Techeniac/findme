//
//  TrackingStatusModel.swift
//  SwiftUI_Base
//
//  Created by Techeniac Services on 05/10/21.
//

import Foundation
struct TrackingStatusModel: Hashable, Identifiable {
    let id = UUID()
    var status: String
    var time: String
}

struct trackingStatusData {
    static var TrackingStatusList = [TrackingStatusModel(status: "Your request has been accepted.", time: "04:30 PM, 19 June 2021"),
                                     TrackingStatusModel(status: "Your service provider is on the way.", time: "05:30 PM, 19 June 2021"),
                                     TrackingStatusModel(status: "Your service provider is arrived.", time: "7:10 PM, 19 June 2021"),
                                     TrackingStatusModel(status: "Work in progress.", time: "02:10 AM, 20 June 2021"),
                                     TrackingStatusModel(status: "Your work done.", time: "12:30 PM, 21 June 2021")]
}

