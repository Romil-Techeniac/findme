//
//  UserModel.swift
//  SwiftUI_Base
//
//  Created by Techeniac Services on 26/10/21.
//

import Foundation
import ObjectMapper
class usr: Object, Mappable {
    
    @objc dynamic var about: String = ""
    @objc dynamic var appNo: String = ""
     var certificate: [Any] = []
    @objc dynamic var city: String = ""
    @objc dynamic var country: String = ""
    @objc dynamic var countryCode: String = ""
    @objc dynamic var coverPhoto: String = ""
    @objc dynamic var dob: String = ""
    @objc dynamic var email: String = ""
    @objc dynamic var firstName: String = ""
    @objc dynamic var gender: String = ""
    @objc dynamic var gmtTime: String = ""
    @objc dynamic var headline: String = ""
    @objc dynamic var houseNo: String = ""
    @objc dynamic var id: Int = 0
    @objc dynamic var idProof: String = ""
    @objc dynamic var isAvailable: Int = 0
    @objc dynamic var isBlock: Int = 0
    @objc dynamic var isEmailVerified: Int = 0
    @objc dynamic var isLogin: Int = 0
    @objc dynamic var isMobileVerified: Int = 0
    @objc dynamic var isPasswordReset: Int = 0
    @objc dynamic var isTermCondition: Int = 0
    @objc dynamic var isVaccination: Int = 0
    @objc dynamic var lastName: String = ""
    @objc dynamic var lat: String = ""
    @objc dynamic var lng: String = ""
    @objc dynamic var mobileNumber: String = ""
    @objc dynamic var paypalVerifiedAccount: Int = 0
    @objc dynamic var perHourPrice: Int = 0
    @objc dynamic var profilePicture: String = ""
    @objc dynamic var review: Int = 0
    @objc dynamic var serviceCharge: Int = 0
    @objc dynamic var state: String = ""
    @objc dynamic var street: String = ""
    @objc dynamic var timeZone: String = ""
    @objc dynamic var trainingPlaceId: Int = 0
    @objc dynamic var vaccinationCertificate: String = ""
    @objc dynamic var video: String = ""
    @objc dynamic var walletAmount: Int = 0
    @objc dynamic var yearOfExperience: String = ""
    @objc dynamic var zipcode: String = ""
    
    required convenience init?(map: Map) {
        self.init()
    }
    override class func primaryKey() -> String? {
        return "id"
    }
    func mapping(map: Map) {
        
        
        about                           <- map["about"]
        appNo                           <- map["app_no"]
        certificate                     <- map["certificate"]
        city                            <- map["city"]
        country                         <- map["country"]
        countryCode                     <- map["country_code"]
        coverPhoto                      <- map["cover_photo"]
        dob                             <- map["dob"]
        email                           <- map["email"]
        firstName                       <- map["first_name"]
        gender                          <- map["gender"]
        gmtTime                         <- map["gmt_time"]
        headline                        <- map["headline"]
        houseNo                         <- map["house_no"]
        id                              <- map["id"]
        idProof                         <- map["id_proof"]
        isAvailable                     <- map["is_available"]
        isBlock                         <- map["is_block"]
        isEmailVerified                 <- map["is_email_verified"]
        isLogin                         <- map["is_login"]
        isMobileVerified                <- map["is_mobile_verified"]
        isPasswordReset                 <- map["is_password_reset"]
        isTermCondition                 <- map["is_term_condition"]
        isVaccination                   <- map["is_vaccination"]
        lastName                        <- map["last_name"]
        lat                             <- map["lat"]
        lng                             <- map["lng"]
        mobileNumber                    <- map["mobile_number"]
        paypalVerifiedAccount           <- map["paypal_verified_account"]
        perHourPrice                    <- map["per_hour_price"]
        profilePicture                  <- map["profile_picture"]
        review                          <- map["review"]
        serviceCharge                   <- map["service_charge"]
        state                           <- map["state"]
        street                          <- map["street"]
        timeZone                        <- map["time_zone"]
        trainingPlaceId                 <- map["training_place_id"]
        vaccinationCertificate          <- map["vaccination_certificate"]
        video                           <- map["video"]
        walletAmount                    <- map["wallet_amount"]
        yearOfExperience                <- map["year_of_experience"]
        zipcode                         <- map["zipcode"]
    }
}
