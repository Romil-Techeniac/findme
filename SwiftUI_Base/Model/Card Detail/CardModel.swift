//
//  CardModel.swift
//  SwiftUI_Base
//
//  Created by Techeniac Services on 14/10/21.
//

import Foundation
struct CardModel: Hashable, Identifiable {
    let id = UUID()
    var isSelected : Bool = false
    var type: CardType = .Other
    var number : String
    var name : String
    var CVV : String
}

struct CardData {
    static let sampleData = AddressModel(type: .Home, houseNo: "D-120", buildingName: "Titanium Square", locality: "Thaltej", city: "Ahmedabad", state: "Gujarat", pincode: "395006")
    static var CardList =
        [
            CardModel(isSelected: true, type: .Visa, number: "4444 4444 4444 4444", name: "Romil Dhanani", CVV: "344"),
//            CardModel(isSelected: false, type: .Master, number: "2345 4512 5494 1221", name: "Ronak shiva", CVV: "123"),
            CardModel(isSelected: false, type: .Master, number: "2345 4512 5494 1221", name: "Ronak shiva", CVV: "123")
        ]
}

enum CardType :String, Equatable, CaseIterable{
    case Master
    case Visa
    case Other
    
    var description : String{
        switch self {
        case .Master: return "Master"
        case .Visa: return "Visa"
        case .Other: return "Other"
        }
    }
}
