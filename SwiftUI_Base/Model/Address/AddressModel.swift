//
//  AddressModel.swift
//  SwiftUI_Base
//
//  Created by Techeniac Services on 07/10/21.
//

import Foundation
struct AddressModel: Hashable, Identifiable {
    let id = UUID()
    var isSelected : Bool = false
    var type: AddressType = .Home
    var houseNo: String
    var buildingName: String
    var locality: String
    var city: String
    var state: String
    var pincode: String
}

struct AddressData {
    static let sampleData = AddressModel(type: .Home, houseNo: "D-120", buildingName: "Titanium Square", locality: "Thaltej", city: "Ahmedabad", state: "Gujarat", pincode: "395006")
    static var addressList =
        [
            AddressModel(type: .Work, houseNo: "as-120", buildingName: "Titanium Square", locality: "Thaltej", city: "Ahmedabad", state: "Gujarat", pincode: "395006"),
            AddressModel(type: .Work, houseNo: "102", buildingName: "iscon Banglow", locality: "Thaltej", city: "Ahmedabad", state: "Gujarat", pincode: "395006"),
            AddressModel(type: .Home, houseNo: "D-1", buildingName: "Rajpath Club", locality: "Thaltej", city: "Ahmedabad", state: "Gujarat", pincode: "395006"),
            AddressModel(type: .Office, houseNo: "a-78", buildingName: "Aron Spectra", locality: "Thaltej", city: "Ahmedabad", state: "Gujarat", pincode: "395006"),
            AddressModel(type: .Home, houseNo: "45", buildingName: "Jay ambe naagar", locality: "Thaltej", city: "Ahmedabad", state: "Gujarat", pincode: "395006"),
            AddressModel(type: .Office, houseNo: "458", buildingName: "Rajhansh ", locality: "Thaltej", city: "Ahmedabad", state: "Gujarat", pincode: "395006")
        ]
}

enum AddressType :String, Equatable, CaseIterable{
    case Home
    case Office
    case Work
    
    var description : String{
        switch self {
        case .Home: return "Home"
        case .Office: return "Office"
        case .Work: return "Work"
        }
    }
}
