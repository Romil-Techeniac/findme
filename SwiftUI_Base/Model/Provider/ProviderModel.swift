//
//  ProviderModel.swift
//  SwiftUI_Base
//
//  Created by Techeniac Services on 05/10/21.
//

import Foundation
struct ProviderModel: Hashable, Identifiable {
    let id = UUID()
    //    var image: String = "work_1"
    //    var name: String = "Plumbing Service"
    
    var isTopRated : Bool = true
    var isNotification : Bool = false

    var providerImage : String = "work_1"
    var providerName : String = "VR Mechanic"
    var providerEmail : String = "VR_Mechanic@popu.com"
    var providerCategory : String = "Car Repair & Service Center"
    var providerPricePerHours : String = "30"
    var providerRateing : String = "4.33"
    var providerExperiance : String = "11"
    var providerLocation : String = "Ahmedabad, Surat"
    var providerSuccsessRate : String = "95"
    var providerJobs : String = "141"
    var providerContactNumber : String = "+91 8511231458"
    var providerChatID : String = "CHAT_123456"
    var providerLat : String = "45.0"
    var providerLong : String = "12.5"
    var providerAboutCare : String = "The solutions offer 24/7 services without any break. So, let’s have a discussion on the working of this system. As discussed above, a lot of different professionals are always available for customers to fix their problems."
//    var provider : String = "11"


    
}
struct ProviderData {
    static let sampleData = ProviderModel(isTopRated: true, providerImage: "work_1", providerName: "Vatsal Makwana", providerCategory: "Car Repair & Service Center", providerPricePerHours: "30", providerRateing: "4.2", providerExperiance: "5", providerLocation: "Ahmedabad", providerSuccsessRate: "67", providerJobs: "120", providerContactNumber: "+91 9879874567",providerAboutCare: "The solutions offer 24/7 services without any break. So, let’s have a discussion on the working of this system. As discussed above, a lot of different professionals are always available for customers to fix their problems.")

    static var ProviderList = [
        ProviderModel(isTopRated: true, providerImage: "work_1", providerName: "VR Mechanic", providerCategory: "Car Repair & Service Center", providerPricePerHours: "30", providerRateing: "4.2", providerExperiance: "5", providerLocation: "Ahmedabad", providerSuccsessRate: "67", providerJobs: "120", providerContactNumber: "+91 9879874567"),
        ProviderModel(isTopRated: true, providerImage: "work_2", providerName: "Shiva Datta", providerCategory: "Painter", providerPricePerHours: "31", providerRateing: "2.2", providerExperiance: "7", providerLocation: "Baroda", providerSuccsessRate: "58", providerJobs: "114", providerContactNumber: "+91 8547878457"),
        ProviderModel(isTopRated: true, providerImage: "work_1", providerName: "Raju Shetty", providerCategory: "Car Painter", providerPricePerHours: "20", providerRateing: "1.2", providerExperiance: "1", providerLocation: "Bombay", providerSuccsessRate: "45", providerJobs: "17", providerContactNumber: "-"),
        ProviderModel(isTopRated: true, providerImage: "work_2", providerName: "Pandit", providerCategory: "Pan Center", providerPricePerHours: "5", providerRateing: "4.9", providerExperiance: "9", providerLocation: "Iscon Mall", providerSuccsessRate: "98", providerJobs: "1204", providerContactNumber: "+91 8899889985")
//        ProviderModel(isTopRated: true, providerImage: "work_1", providerName: "VR Mechanic", providerCategory: "Car Repair & Service Center", providerPricePerHours: "30", providerRateing: "4.2", providerExperiance: "5", providerLocation: "Ahmedabad", providerSuccsessRate: "67", providerJobs: "120", providerContactNumber: "+91 9879874567"),
                               
    ]
}
