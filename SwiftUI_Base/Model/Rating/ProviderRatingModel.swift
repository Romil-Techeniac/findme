//
//  ProviderRatingModel.swift
//  SwiftUI_Base
//
//  Created by Techeniac Services on 22/10/21.
//

import Foundation
struct ProviderRatingModel: Hashable, Identifiable {
    let id = UUID()
    var providerImage : String
    var providerName : String
    var providerCategory : String
    var providerPricePerHours : String
    var providerTotalRateing : String
    var providerTotalRateingCount : String
    var provider1StarRateing : Int
    var provider2StarRateing : Int
    var provider3StarRateing : Int
    var provider4StarRateing : Int
    var provider5StarRateing : Int
    var providerExperiance : String
    var SelectedProiderRatingData : [SelectedProiderRatingModel]
    
}

struct ProviderRatingData {
    static let sampleData = ProviderRatingModel(providerImage: "work_1", providerName: "VR Mechanic", providerCategory: "Car Repair & Service Center", providerPricePerHours: "30", providerTotalRateing: "4.33", providerTotalRateingCount: "45", provider1StarRateing: 10, provider2StarRateing: 40, provider3StarRateing: 20,provider4StarRateing: 10,provider5StarRateing: 20, providerExperiance: "11", SelectedProiderRatingData: SelectedProiderRatingData.SelectedProiderRatingDataList)
    static var ProviderRatingList = [
        ProviderRatingModel(providerImage: "work_1", providerName: "VR Mechanic", providerCategory: "Car Repair & Service Center", providerPricePerHours: "30", providerTotalRateing: "4.33", providerTotalRateingCount: "12", provider1StarRateing: 10, provider2StarRateing: 40, provider3StarRateing: 20,provider4StarRateing: 10,provider5StarRateing: 20, providerExperiance: "11", SelectedProiderRatingData: SelectedProiderRatingData.SelectedProiderRatingDataList),
        ProviderRatingModel(providerImage: "work_1", providerName: "VR Mechanic", providerCategory: "Car Repair & Service Center", providerPricePerHours: "30", providerTotalRateing: "4.33", providerTotalRateingCount: "13", provider1StarRateing: 10, provider2StarRateing: 40, provider3StarRateing: 20,provider4StarRateing: 10,provider5StarRateing: 20, providerExperiance: "11", SelectedProiderRatingData: SelectedProiderRatingData.SelectedProiderRatingDataList),
        ProviderRatingModel(providerImage: "work_1", providerName: "VR Mechanic", providerCategory: "Car Repair & Service Center", providerPricePerHours: "30", providerTotalRateing: "4.33", providerTotalRateingCount: "78", provider1StarRateing: 10, provider2StarRateing: 40, provider3StarRateing: 20,provider4StarRateing: 10,provider5StarRateing: 20, providerExperiance: "11", SelectedProiderRatingData: SelectedProiderRatingData.SelectedProiderRatingDataList),
        ProviderRatingModel(providerImage: "work_1", providerName: "VR Mechanic", providerCategory: "Car Repair & Service Center", providerPricePerHours: "30", providerTotalRateing: "4.33", providerTotalRateingCount: "49", provider1StarRateing: 10, provider2StarRateing: 40, provider3StarRateing: 20,provider4StarRateing: 10,provider5StarRateing: 20, providerExperiance: "11", SelectedProiderRatingData: SelectedProiderRatingData.SelectedProiderRatingDataList)
                               
    ]
}



struct SelectedProiderRatingModel: Hashable, Identifiable {
    let id = UUID()
    var image : String
    var name : String
    var rateing : String
    var description : String
    var time = Date()
}
struct SelectedProiderRatingData {
    static let sampleData = SelectedProiderRatingModel(image: "work_1", name: "VR Mechanic", rateing: "4.33", description: "This chair is great addition in any room in your home, not only in the living room, featuring the mid-century design with modern trend and with that I’m fully satisfied with their quality services and material they use to make things approachable and ...", time: Date().addingTimeInterval(19800))
    static var SelectedProiderRatingDataList = [
        SelectedProiderRatingModel(image: "work_1", name: "VR Mechanic", rateing: "4.33", description: "This chair is great addition in any room in your home, not only in the living room, featuring the mid-century design with modern trend and with that I’m fully satisfied with their quality services and material they use to make things approachable and ...", time: Date()),
        SelectedProiderRatingModel(image: "work_1", name: "VR Mechanic", rateing: "4.33", description: "This chair is great addition in any room in your home, not only in the living room, featuring the mid-century design with modern trend and with that I’m fully satisfied with their quality services and material they use to make things approachable and ...", time: Date().addingTimeInterval(1800)),
        SelectedProiderRatingModel(image: "work_1", name: "VR Mechanic", rateing: "4.33", description: "This chair is great addition in any room in your home, not only in the living room, featuring the mid-century design with modern trend and with that I’m fully satisfied with their quality services and material they use to make things approachable and ...", time: Date().addingTimeInterval(29800)),
        SelectedProiderRatingModel(image: "work_1", name: "VR Mechanic", rateing: "4.33", description: "This chair is great addition in any room in your home, not only in the living room, featuring the mid-century design with modern trend and with that I’m fully satisfied with their quality services and material they use to make things approachable and ...", time: Date().addingTimeInterval(45800))
                               
    ]
}
