//
//  SearchModel.swift
//  SwiftUI_Base
//
//  Created by Techeniac Services on 05/10/21.
//

import Foundation
struct SearchModel: Hashable, Identifiable {
    let id = UUID()
    var image: String
    var name: String
}

struct searchData {
    static var searchList = [ProviderModel(providerImage: "work_1", providerName: "VR Mechanic", providerCategory: "Car Repair & Service Center", providerPricePerHours: "30", providerRateing: "1.2"),
                             ProviderModel(providerImage: "work_2", providerName: "Shiva Datta", providerCategory: "Painter", providerPricePerHours: "20", providerRateing: "4.5"),
                             ProviderModel(providerImage: "work_1", providerName: "Raju PanWala", providerCategory: "Pan Center", providerPricePerHours: "25", providerRateing: "5.0")]
}
