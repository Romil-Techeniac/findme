//
//  CategoryModel.swift
//  SwiftUI_Base
//
//  Created by Techeniac Services on 05/10/21.
//

import Foundation
struct CategoryModel: Hashable, Identifiable {
    let id = UUID()
    var image: String
    var name: String
}

struct CategoryData {
    static var categoryList = [CategoryModel(image: "work_1", name: "Cleaning"),
                               CategoryModel(image: "work_1", name: "Mechanic"),
                               CategoryModel(image: "work_1", name: "Painting"),
                               CategoryModel(image: "work_1", name: "Plumbing"),
                               CategoryModel(image: "work_1", name: "Electrical"),
                               CategoryModel(image: "work_1", name: "More")]
}

